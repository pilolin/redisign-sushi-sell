const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [
	new HtmlWebpackPlugin({
		template: './src/index.pug',
		filename: './index.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/basket.pug',
		filename: './basket.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/checkout.pug',
		filename: './checkout.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/checkout-final.pug',
		filename: './checkout-final.html'
	}),
]