$(document).ready(function () {
    $(".type-delivery-link").click(function () {
        var data = $(this).attr('data-yourself');
        if (data == 2) {
            $('#deliveryform-type_yourself').attr('value', 2);
            $('.cities_tab, .address-form2').hide();
            if ($('a[data-order_type=2]').hasClass('pay-tab_link--active')) {
                $('.tab-pickups').hide();
                $('.tab-pickups').removeClass('tab-content--active');
            } else if ($('a[data-order_type=1]').hasClass('pay-tab_link--active')) {
                $('.tab-pickups').show();
                $('.tab-pickups').add('tab-content--active');
            }
            $('.discount_block').show();
        }
        if (data == 1) {
            $('#deliveryform-type_yourself').attr('value', 1);
            $('.discount_block').hide();
            if ($('a[data-order_type=2]').hasClass('pay-tab_link--active')) {
                $('.cities_tab, .address-form2').hide();
                $('.tab-pickups').hide();
                $('.tab-pickups').removeClass('tab-content--active');
            } else if ($('a[data-order_type=1]').hasClass('pay-tab_link--active')) {
                $('.cities_tab, .address-form2').show();
                $('.tab-pickups').hide();
                $('.tab-pickups').removeClass('tab-content--active');
            }
        }
    });

    $('.take-list_choose').click(function () {
        $('.take-list_choose').removeClass('take_list_choose_active');
        $(this).addClass('take_list_choose_active');
        var pickup_id = $(this).data('id');
        $('#deliveryform-affiliate_id').attr('value', pickup_id);

        return false;
    });

    $('.cities_tab a.pay-tab_link').on('click', function () {
        var city_id = $(this).attr('data-city_id');
        if (city_id) {
            $('#deliveryform-city').attr('value', city_id);
        }
    });

    var itemCount = $('.basket-icon_num'),
        itemCountCnt = itemCount.text();

    $('.magnific-popup').on('click', function () {
        var id = $(this).data('id'),
            attr = $(this).attr('data-url'),
            title = $(this).parent().parent().find('.product-item__name'),
            price = $(this).parent().parent().find('.product-item__price'),
            descr = $(this).parent().parent().find('.product-full-comment'),
            portion = $(this).parent().parent().find('.product-item__portion'),
            dataPrice = $(this).parent().parent().find('.product-item__btn').attr('data-price');
        $('.product-item__img--modal img').attr("src", attr);
        $('.product-item__name--modal').text(title.text());
        $('.product-item__price--modal').html(price.html());
        $('.product-item__components--modal').text(descr.text());
        $('.product-item__portion--modal').html(portion.html());
        $('.product-item__btn--modal').attr('data-price', dataPrice);
        $('.product-item__btn--modal').attr('data-id', id);
    });

    var basketCnt = 0;
    var basketSum = Number($('.basket-prods_total').data('sum'));

    function enableClick() {
        $('.basket-prods_cnt').css("pointer-events", "auto");
    }

    $('ul.basket-prods_list').on('click', '.basket-prods_cnt--plus', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var composite = Number($(this).parent().parent().attr('data-iscomposite'));
        if ((composite == 1) && (document.location.pathname != '/cart/purchase/')) {
            $('a.composite_modal_link_basket[data-id=' + id + ']').click();
            return;
        }
        basketCnt = parseInt($(this).parent().find('.basket-prods_num').text());
        $(this).parent().find('.basket-prods_num').text(++basketCnt);
        $('span.purchase-offers_count--digit[data-id=' + id + ']').text(basketCnt);
        var price = Number($(this).parent().parent().find('.basket-prods_price').text().trim());
        basketSum += price;
        $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
        $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
        $('.purchase-offers_count--digit[data-id=' + id + ']').text(basketCnt);
        addCount(price - devices_price_);
        addItem();
        changeBar(priceCountCnt);
        $('.basket-prods_cnt').css("pointer-events", "none");
        setTimeout(enableClick, 1000);
        getContextRules_();
        recalcDevicesPrice();
    });

    $('ul.basket-prods_list').on('click', '.basket-prods_cnt--minus', function (e) {
        e.preventDefault();
        basketCnt = parseInt($(this).parent().find('.basket-prods_num').text());
        if (basketCnt == 0) {
            return;
        }

        var id = $(this).data('id');

        basketCnt = $(this).parent().find('.basket-prods_num').text();
        $(this).parent().find('.basket-prods_num').text(--basketCnt);
        $('span.purchase-offers_count--digit[data-id=' + id + ']').text(basketCnt);
        var price = Number($(this).parent().parent().find('.basket-prods_price').text().trim());
        basketSum -= price;
        $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
        $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
        $('.purchase-offers_count--digit[data-id=' + id + ']').text(basketCnt);
        var food_count = Number($(this).parent().find('.basket-prods_num').text().trim());
        $('.basket-prods_cnt').css("pointer-events", "none");
        setTimeout(enableClick, 1000);
        removeCount(price + devices_price_);
        removeItem();
        changeBar(priceCountCnt);
        checkSubmit();
        if (food_count == 0) {
            $(this).parent().parent().remove();
            $('.purchase-offers_item[data-id=' + id + ']').remove();
        }
        getContextRules_();
        recalcDevicesPrice();
    });

    var priceCount = $('.basket-icon_digits'),
        priceCountCnt = +Number($('.basket-icon_digits').text());


    $('.basket-prods_cnt').on('click', function (e) {
        e.preventDefault();
        $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
    });

    $('ul.basket-prods_list').on('click', '.basket-prods_close', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var price = Number($(this).parent().find('.basket-prods_price').text().trim());
        var food_count = Number($(this).parent().find('.basket-prods_num').text().trim());
        var request;
        $.ajax({
            type: "POST",
            url: "/cart/del",
            data: {
                id: id
            },
            async: false,
            success: function (msg) {
                if (msg != 'undefined') {
                    $('li.gift_basket').remove();
                    $('li.gift_offer').remove();
                    var request = JSON.parse(msg);
                    if (request) {
                        $('ul.basket-prods_list').append(request['gift_basket']);
                    }
                }
            }
        })
        if (request == 'undefined') {
            return false;
        }
        basketSum -= food_count * price;
        $(this).parent('.basket-prods_item').remove();
        removeCount(food_count * price + devices_price_);
        if (food_count > 1) {
            for (var i = 0; i < food_count; i++) {
                removeItem();
            }
        } else {
            removeItem();
        }
        changeBar(priceCountCnt);
        checkSubmit();
        $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
        $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
        $('.purchase-offers_item[data-id=' + id + ']').remove();
        getContextRules_();
        recalcDevicesPrice();
        $('a.composite_food_add[data-id=' + id + ']').parent().parent().remove();
    });

    var cntDigit = 1;
    $('ul.purchase-offers_list').on('click', '.purchase-offers_count--plus', function () {
        var id = $(this).data('id');
        var composite = Number($(this).parent().parent().parent().attr('data-iscomposite'));
        var price = Number($(this).data('price'));
        basketCnt = parseInt($('li.basket-prods_item[data-id=' + id + ']').find('span.basket-prods_num').text());
        $('li.basket-prods_item[data-id=' + id + '] span.basket-prods_num').text(++basketCnt);
        basketSum += price;
        $(this).parent().find('.purchase-offers_count--digit').text(basketCnt);
        $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
        $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
        addCount(price - devices_price_);
        addItem();
        changeBar(priceCountCnt);
        $('.basket-prods_cnt').css("pointer-events", "none");
        setTimeout(enableClick, 1000);
        getContextRules_();
        recalcDevicesPrice();
    });

    $('ul.purchase-offers_list').on('click', '.purchase-offers_count--minus', function () {
        var id = $(this).data('id');
        var composite = Number($(this).parent().parent().parent().attr('data-iscomposite'));
        var price = Number($(this).data('price'));
        basketCnt = parseInt($('li.basket-prods_item[data-id=' + id + ']').find('span.basket-prods_num').text());
        $('li.basket-prods_item[data-id=' + id + '] span.basket-prods_num').text(--basketCnt);
        basketSum -= price;
        $(this).parent().find('.purchase-offers_count--digit').text(basketCnt);
        $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
        $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
        removeCount(price + devices_price_);
        removeItem();
        var food_count = Number($(this).parent().find('.purchase-offers_count--digit').text().trim());
        changeBar(priceCountCnt);
        checkSubmit();
        if (food_count == 0) {
            $('.purchase-offers_item[data-id=' + id + ']').remove();
            $('.basket-prods_item[data-id=' + id + ']').remove();
        }
        $('.basket-prods_cnt').css("pointer-events", "none");
        setTimeout(enableClick, 1000);
        getContextRules_();
        recalcDevicesPrice();
    });

    $('ul.tools-list').on('click', '.purchase-offers_count--plus', function () {
        var id = $(this).data('id');
        var dataPrice = $(this).data('price');
        var qty = Number($(this).parent().find('.purchase-offers_count--digit').text());
        $.ajax({
            type: "POST",
            url: '/cart/change_devices_count',
            data: {
                id: id,
                action_: 'plus'
            },
            async: false,
            success: function (request) {
                request_price = JSON.parse(request);
                devices_price_ = request_price;
            }
        });
        if (basketSum >= 0) {
            basketSum += dataPrice;
            $(this).parent().find('.purchase-offers_count--digit').text(++qty);
            $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
            $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
            addCount(dataPrice);
            changeBar(priceCountCnt);
        }
    });

    $('ul.tools-list').on('click', '.purchase-offers_count--minus', function () {
        var id = $(this).data('id');
        var dataPrice = $(this).data('price');
        var qty = Number($(this).parent().find('.purchase-offers_count--digit').text());
        var th = $(this);
        $.ajax({
            type: "POST",
            url: '/cart/change_devices_count',
            data: {
                id: id,
                action_: 'minus'
            },
            async: false,
            success: function (request) {
                request_price = JSON.parse(request);
                if (request_price['qty'] <= qty - 1) {
                    devices_price_ = request_price['d_price'];
                    if (basketSum > 0) {
                        basketSum -= dataPrice;
                        th.parent().find('.purchase-offers_count--digit').text(--qty);
                        $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
                        $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
                        removeCount(dataPrice);
                        changeBar(priceCountCnt);
                    }
                }
            }
        });
    })

    $(document).on('click', '.purchase-btn', function () {

        var id = $(this).data('id');
        var composite = Number($(this).parent().parent().attr('data-iscomposite'));
        var dataPrice = $(this).data('price');
        var cat_id = $(this).data('cat');
        var title = $(this).parent().find('.drinks-list_title').text();
        var image_url = $(this).parent().parent().find('img').attr('src');
        var big_image_url = $(this).data('imagebig');
        var isset = false;
        $('ul.basket-prods_list li').each(function (index, element) {
            var current_id = $(this).attr('data-id');
            if (current_id == id) {
                isset = true;
            }
        });
        if (isset == true) {
            $('span.basket-prods_cnt--plus[data-id=' + id + ']').click();
        } else {
            var request;
            $.ajax({
                type: "POST",
                url: "/cart/add",
                data: {
                    id: id,
                    type_render: 2
                },
                async: false,
                success: function (request) {
                    if (request != 'undefined') {
                        var request_array = JSON.parse(request);
                        $('ul.basket-prods_list').prepend(request_array['row1']);
                        $('ul.purchase-offers_list').prepend(request_array['row2']);
                        $('li.gift_basket').remove();
                        $('li.gift_offer').remove();
                        if (request_array['gift_basket']) {
                            $('ul.basket-prods_list').append(request_array['gift_basket']);
                            if (request_array['gift_offer']) {
                                $('ul.purchase-offers_list').append(request_array['gift_offer']);
                            }
                        }
                    }
                }
            });
        }

        var _this = $(this),
            elBasket = $('.header-bottom_basket'),
            elBasketOffset = elBasket.offset();
        if ($(window).scrollTop() > HeaderTop) {
            elBasketOffset = {
                top: elBasket.offset().top - 90,
                left: elBasket.offset().left
            };
        }
        parent = _this.closest('li'),
            img = parent.find('.product-item__img'),
            imgOffset = img.offset(),
            imgClone = img.clone().addClass('animate').css({
                position: 'absolute',
                zIndex: 9999,
                top: imgOffset.top + "px",
                left: imgOffset.left + "px",
                width: img.width()
            });

        var price = +$(this).attr('data-price');

        $('body').append(imgClone).find('.product-item__img.animate').animate({
            top: elBasketOffset.top + (elBasket.height() / 2),
            left: elBasketOffset.left + (elBasket.width() / 2),
            height: "hide",
            width: "hide",
        }, {
            duration: 300,
            complete: function () {
                $(this).remove();
            }
        });

        if (isset == false) {
            addCount(price - devices_price_);
            addItem();
            changeBar(priceCountCnt);
            basketSum += price;
            $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
            $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
            checkSubmit();
            getContextRules_();
            recalcDevicesPrice();
        }
        return false;
    });

    $('a.purchase-form_btn').on('click', function (e) {
        e.preventDefault();
        var current_obj = $(this);
        $('.composite_alert_text').remove();
        $('li.purchase-offers_item').removeClass('composite_food_alert');

        $.ajax({
            type: "POST",
            url: '/cart/testcompositefoods',
            async: false,
            success: function (msg) {
                if (msg == 1) {
                    window.location = '/cart/delivery/';
                } else {
                    var request_array = JSON.parse(msg);
                    if (!($.isEmptyObject(request_array))) {
                        var chosen_composite_foods = [];
                        for (var i in request_array) {
                            for (var j in request_array[i]) {
                                chosen_composite_foods.push(j);
                            }
                        }
                        var show_alert_message = false;
                        $('.composite_food_add').each(function () {
                            var composite_id = $(this).attr('data-composite_id');
                            if (chosen_composite_foods.indexOf(composite_id) == -1) {
                                show_alert_message = true;
                                $(this).parent().parent().addClass('composite_food_alert');
                            }
                        });
                        if (show_alert_message) {
                            current_obj.parent().parent().append("<h2 class='composite_alert_text'>Укажите состав у всех наборов!</h2>");
                        }
                    }
                }
            }
        });

    });

    $('.pay-tab_link').on('click', function (e) {
        e.preventDefault();
        var pay_type = $(this).data('pay_type');
        $('#payform-pay_type').attr('value', pay_type);
        $(this)
            .addClass('pay-tab_link--active')
            .parent().siblings().find('.pay-tab_link')
            .removeClass("pay-tab_link--active")
            .closest(".pay-wrap")
            .find(".pay-total")
            .removeClass("pay-total--active")
            .eq($(this).parent().index())
            .addClass("pay-total--active");
    });

    $('.submit-tab_link').on('click', function (e) {
        e.preventDefault();
        $(this)
            .addClass('pay-tab_link--active')
            .parent().siblings().find('.submit-tab_link')
            .removeClass("pay-tab_link--active")
            .closest(".submit")
            .find(".tab-content")
            .removeClass("tab-content--active")
            .eq($(this).parent().index())
            .addClass("tab-content--active");
    });

    $(document).on('click', '.drinks-modal_close', function (e) {
        if (saveComposite() == false) {
            $.magnificPopup.close();
        }
        return false
    });

    $(document).on('click', '.composite-modal_close', function (e) {
        $.magnificPopup.close();
        choose_composite_count = 0;
        return false;
    });

    var choose_composite_count = 0;

    $(document).on('click', '.save_composite', function () {
        saveComposite();
        return false;
    });

    function saveComposite() {
        var foods_id_arr = [];
        $('.btn_composite_item_add--select').each(function () {
            var id = $(this).data('id');
            foods_id_arr.push(id);
        });
        if (foods_id_arr.length > 0) {
            var id = $('ul.drinks-list').attr('data-id');
            var composite_id = $('ul.drinks-list').attr('data-composite_id');
            var composite_count = $('ul.drinks-list').attr('data-composite_count');
            var foods_id_str = foods_id_arr.join(',');
            if (composite_count == choose_composite_count) {
                $.ajax({
                    type: "POST",
                    url: "/cart/addcompositeitem",
                    data: {
                        id: id,
                        foods_id_str: foods_id_str,
                        composite_id: composite_id
                    },
                    async: false,
                    success: function (msg) {
                        if (msg) {
                            $('a.composite_food_add[data-composite_id=' + composite_id + ']').parent().parent().removeClass('composite_food_alert');
                            $.magnificPopup.close();
                        }
                    }
                });
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    function saveComposite_(id, composite_id, foods_id_str) {
        $('button.product-item__btn[data-id=' + id + ']').attr('data-iscomposite', 0);
        $('li.basket-prods_item[data-id=' + id + ']').attr('data-iscomposite', 0);
        if (document.location.pathname == '/shop/bonus' ||
            document.location.pathname == '/shop/payment' ||
            document.location.pathname.indexOf('/shop/') == -1) {
            $('span.basket-prods_cnt--plus[data-id=' + id + ']').click();
        } else {
            $('button.product-item__btn[data-id=' + id + ']').click();
        }
        $('button.product-item__btn[data-id=' + id + ']').attr('data-iscomposite', 1);
        $('li.basket-prods_item[data-id=' + id + ']').attr('data-iscomposite', 1);
        $.ajax({
            type: "POST",
            url: "/cart/addcompositeitem",
            data: {
                id: id,
                foods_id_str: foods_id_str,
                composite_id: composite_id
            },
            async: false,
            success: function (msg) {
                if (msg) {
                    $.magnificPopup.close();
                }
            }
        });
    }

    $(document).on('click', '.drinks-modal_hide', function (e) {
        e.preventDefault();
        $.magnificPopup.close();

    });

    $(document).on('click', 'a.product-item__btn--modal', function (e) {
        e.preventDefault();
        if (!$(this).attr('disabled')) {
            var id = $(this).attr('data-id');
            $.magnificPopup.close();
            $('button.product-item__btn[data-id=' + id + ']').trigger('click');
        }
        return false;
    });

    $('.purchase-popup').magnificPopup({
        type: 'inline',
        preloader: false,
        modal: true
    });


    $('.purchase-offers_add').magnificPopup({
        type: 'inline',
        preloader: false,
        modal: true
    });

    $('.composite_food_add').magnificPopup({
        type: 'inline',
        preloader: false,
        modal: true
    });

    $('.composite_modal_link').magnificPopup({
        type: 'inline',
        preloader: false,
        modal: true
    });

    $('.composite_modal_link_basket').magnificPopup({
        type: 'inline',
        preloader: false,
        modal: true
    });


    $(document).on('click', 'a.composite_food_add', function (e) {
        e.preventDefault();
        $('.drinks-modal_hide').hide();
        $('.save_composite').show();
        $('ul.drinks-list').removeAttr('data-composite_id');
        $('ul.drinks-list').removeAttr('data-id');
        $('ul.drinks-list').removeAttr('data-composite_count');
        $('ul.drinks-list').find('li.drinks-list_item').remove();
        $('ul.drinks-list span.chosen_composite_food').remove();
        var id = $(this).data('id');
        var foods_str = $(this).data('foods');
        var composite_id = $(this).data('composite_id');
        var composite_count = $(this).data('composite_count');
        choose_composite_count = 0;
        $.ajax({
            type: "POST",
            url: "/cart/rendercompositeitems",
            data: {
                id: id,
                composite_id: composite_id,
                foods_str: foods_str
            },
            async: false,
            success: function (msg) {
                if (msg) {
                    var request = msg;
                    if (request) {
                        $(this).magnificPopup({
                            type: 'inline',
                            preloader: false,
                            modal: true
                        });
                        $('ul.drinks-list').append(request);
                        $('ul.drinks-list').attr('data-id', id);
                        $('ul.drinks-list').attr('data-composite_id', composite_id);
                        $('ul.drinks-list').attr('data-composite_count', composite_count);
                        $('h2.drinks-modal_title').text('Добавьте блюда в набор (' + composite_count + ')');
                        SelectSaveCompositeItems();
                    }
                }
            }
        });
    });

    $(document).on('click', 'a.btn_composite_item_add', function (e) {
        e.preventDefault();

        var composite_count = Number($(this).parent().parent().attr('data-composite_count'));

        if ($(this).parent().hasClass('btn_composite_item_add--select')) {
            $(this).parent().removeClass('btn_composite_item_add--select');
            choose_composite_count--;
        } else {
            if (composite_count > choose_composite_count) {
                $(this).parent().addClass('btn_composite_item_add--select');
                choose_composite_count++;
            }
        }

    });

    $(document).on('click', '.recommends_item--add', function () {
        var id = $(this).attr('data-id');
        $("a.purchase-offers_add[data-id='" + id + "']").click();
    });

    $(document).on('click', '.purchase-offers_add', function () {
        $('.save_composite').hide();
        $('.drinks-modal_hide').show();
        $('h2.drinks-modal_title').text('Добавьте блюда');
        $('span.chosen_composite_food').remove();
        if (!$(this).data('first')) {
            $(this).magnificPopup({
                type: 'inline',
                preloader: false,
                modal: true
            });
            $(this).attr('data-first', 1);
        } else if (($(this).data('first')) && $(this).data('upsale') != null) {
            var upsale = $(this).data('upsale');
            $.ajax({
                type: "POST",
                url: "/cart/upsale",
                data: {
                    upsale: upsale
                },
                async: false,
                success: function (request) {
                    if (request) {
                        $('ul.drinks-list').find('li.drinks-list_item').remove();
                        $('ul.drinks-list').append(request);
                    }
                },
                error: function () {
                    return false;
                }
            });
        }
    });

    $('ul.product-item button.product-item__btn, .banner-list-btn-food').on('click', function () {
        var id = $(this).attr('data-id');
        if (Number($(this).attr('data-iscomposite')) == 1 && Number($(this).attr('data-composite_count')) > 1) {
            $('a.composite_modal_link[data-id=' + id + ']').click();
            return;
        }
        var isset = false;
        $(document).find('li.basket-prods_item').each(function (index, element) {
            var current_id = $(element).attr('data-id');
            if (current_id == id) {
                isset = true;
            }
        });
        if (isset == true) {
            $('span.basket-prods_cnt--plus[data-id=' + id + ']').click();
        } else {
            var image_url = $(".food-image-" + id).attr('src'),
                title = $(this).parent().find('.product-item__name'),
                price = $(this).parent().find('.product-item__price'),
                descr = $(this).parent().find('.product-item__components'),
                portion = $(this).parent().find('.product-item__portion'),
                dataPrice = $(this).parent().find('.product-item__price').html();
            cat_id = $(this).data('cat');
            var request;
            $('button.product-item__btn').prop("disabled", true);
            $.ajax({
                type: "POST",
                url: "/cart/add",
                data: {
                    id: id,
                    type_render: 1
                },
                async: false,
                success: function (msg) {
                    setTimeout(function(){
                        $('button.product-item__btn').prop("disabled", false);
                    }, 500);
                    if (msg != 'undefined') {
                        var request_array = JSON.parse(msg);
                        $('li.gift_basket').remove();
                        $('ul.basket-prods_list').prepend(request_array['row1']);
                        if (request_array['gift_basket']) {
                            $('ul.basket-prods_list').append(request_array['gift_basket']);
                        }
                    }
                }
            });
        }
        var _this = $(this),
            elBasket = $('.header-bottom_basket'),
            elBasketOffset = elBasket.offset();
        if ($(window).scrollTop() > HeaderTop) {
            elBasketOffset = {
                top: elBasket.offset().top - 90,
                left: elBasket.offset().left
            };
        }
        var parent = _this.closest('li');
        if (_this.parent().parent().hasClass('products-list_item--action')) {
            var img = _this.parent().parent().find('.product-item__img');
            img.show();
        } else {
            var img = parent.find('.product-item__img');
        }
        var imgOffset = img.offset();
        var imgClone = img.clone().addClass('animate').css({
            position: 'absolute',
            zIndex: 9999,
            top: imgOffset.top + "px",
            left: imgOffset.left + "px",
            width: img.width()
        });

        var price = +$(this).attr('data-price');

        $('body').append(imgClone).find('.product-item__img.animate').animate({
            top: elBasketOffset.top + (elBasket.height() / 2),
            left: elBasketOffset.left + (elBasket.width() / 2),
            height: "hide",
            width: "hide",
        }, {
            duration: 300,
            complete: function () {
                $(this).remove();
            }
        });
        if (isset == false) {
            addCount(price);
            addItem();
            changeBar(priceCountCnt);
            basketSum += price;
            $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
            checkSubmit();
            getContextRules_();
        }
        if ($(this).parent().parent().hasClass('products-list_item--action')) {
            $(this).parent().parent().remove();
        }
        return false;
    });

    var HeaderTop = $('header').innerHeight();


    $('.magnific-popup').magnificPopup({
        type: 'inline',
        preloader: false,
        modal: true
    });

    $('.gift-popup').magnificPopup({
        type: 'inline',
        preloader: false,
        modal: true
    });

    $('.footer-policy_link').magnificPopup({
        type: 'inline',
        preloader: false,
        modal: true
    });

    $('.policy_link').magnificPopup({
        type: 'inline',
        preloader: false,
        modal: true
    });

    $('.header-contact_callback').magnificPopup({
        type: 'inline',
        preloader: false,
        modal: true
    });

    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    if ($(window).width() > 768) {};


    $('.js-submit').submit(function (e) {
        e.preventDefault();
        var phone = $(this).find('input[name="phone"]');
        if (phone.val() == "") {
            phone.focus();
            return false;
        } else {
            var form_data = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "/sendmessage.php",
                data: form_data,
                async: false,
                success: function () {}
            });
        }
        return false;
    });

    $('.basket-modal').on('click', '.offer-item_submit', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var composite = Number($(this).attr('data-iscomposite'));
        var cat_id = $(this).data('cat')
        var dataPrice = $(this).data('price');
        var title = $(this).parent().find('.offer-item_title').text();
        var image_url = $(this).parent().find('.offer-item_image img').attr('src');
        var big_image_url = $(this).data('imagebig');

        var isset = false;
        $('ul.basket-prods_list li').each(function (index, element) {
            var current_id = $(this).attr('data-id');
            if (current_id == id) {
                isset = true;
            }
        });
        if (isset == true) {
            $('span.basket-prods_cnt--plus[data-id=' + id + ']').click();
        } else {
            var request;
            $.ajax({
                type: "POST",
                url: "/cart/add",
                data: {
                    id: id,
                    type_render: 2
                },
                async: false,
                success: function (request) {
                    if (request != 'undefined') {
                        var request_array = JSON.parse(request);
                        $('ul.basket-prods_list').append(request_array['row1']);
                        $('ul.purchase-offers_list').prepend(request_array['row2']);
                        $('li.gift_basket').remove();
                        $('li.gift_offer').remove();
                        if (request_array['gift_basket']) {
                            $('ul.basket-prods_list').append(request_array['gift_basket']);
                            if (request_array['gift_offer']) {
                                $('ul.purchase-offers_list').append(request_array['gift_offer']);
                            }
                        }
                    }
                }
            });
        }

        var _this = $(this),
            elBasket = $('.header-bottom_basket'),
            elBasketOffset = elBasket.offset();
        if ($(window).scrollTop() > HeaderTop) {
            elBasketOffset = {
                top: elBasket.offset().top - 90,
                left: elBasket.offset().left
            };
        }
        parent = _this.closest('.offer-item'),
            img = parent.find('.offer-item_image'),
            imgOffset = img.offset(),
            imgClone = img.clone().addClass('animate').css({
                position: 'absolute',
                zIndex: 9999,
                top: imgOffset.top + "px",
                left: imgOffset.left + "px",
                width: img.width()
            });

        var price = +$(this).attr('data-price');

        $('body').append(imgClone).find('.offer-item_image.animate').animate({
            top: elBasketOffset.top + (elBasket.height() / 2),
            left: elBasketOffset.left + (elBasket.width() / 2),
            height: "hide",
            width: "hide",
        }, {
            duration: 300,
            complete: function () {
                $(this).remove();
            }
        });

        if (isset == false) {
            addCount(price - devices_price_);
            addItem();
            changeBar(priceCountCnt);
            basketSum += price;
            $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
            $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
            checkSubmit();
            getContextRules_();
            recalcDevicesPrice();
        }
        return false;
    });


    $('.main-slider').owlCarousel({
        loop: true,
        responsive: {
            0: {
                items: 1,
                mouseDrag: false,
                touchDrag: true
            },
          
        },
        items: 1,
        nav: false,
        navText: '',
        autoplay: true,
        autoplayTimeout: 7000,
        autoplayHoverPause: true,
        dots: true,
    });




    $('a[data-order_type=2]').on('click', function (e) {
        e.preventDefault();
        $('div.vacancy-form_left').css({
            'display': 'inline-block'
        });
        $('.cities_tab, .address-form2').hide();
        $('#deliveryform-order_type').attr('value', 2);
        $('div.delivery-time_content').hide();
        $('div.tab-pickups').removeClass('tab-content--active');
        if ($('a[data-yourself=2]').hasClass('pay-tab_link--active')) {
            $('div.tab-pickups').hide();
            $('.tab-pickups').removeClass('tab-content--active');
            $('.discount_block').show();
        } else {
            $('.tab-pickups').removeClass('tab-content--active');
            $('.discount_block').hide();
        }
    });

    $('a[data-order_type=1]').on('click', function (e) {
        e.preventDefault();
        $('#phone_block').removeAttr('id');
        $('#name_block').removeAttr('id');
        $('div.submit div.submit-1st').show();
        $('div.delivery-time_content').show();
        $('div.vacancy-form_left').css({
            'display': '',
            'margin-left': '',
            'margin-right': ''
        });

        if ($('a[data-yourself=2]').hasClass('pay-tab_link--active')) {
            $('.cities_tab, .address-form2').hide();
            $('div.delivery-agreement').css({
                'display': 'block',
                'margin-left': 'auto',
                'margin-right': 'auto'
            });
            $('#deliveryform-order_type').attr('value', 1);
            $('div.tab-pickups').show();
            $('div.tab-pickups').addClass('tab-content--active');
            $('.discount_block').show();
        } else if ($('a[data-yourself=1]').hasClass('pay-tab_link--active')) {
            $('#deliveryform-order_type').attr('value', 1);
            $('.tab-pickups').hide();
            $('.tab-pickups').removeClass('tab-content--active');
            $('.cities_tab, .address-form2').show();
            $('.discount_block').hide();
        }

    });

    checkSubmit();
    changeBar(priceCountCnt);

    function checkSubmit() {
        if (basketSum > 0) {
            $('a.basket-prods_submit').show();
        } else {
            $('a.basket-prods_submit').hide();
        }
    }

    var devices_price_ = Number($('ul.basket-prods_list').data('devices'));
    if (devices_price_ > 0) {
        basketSum += devices_price_;
    }

    function sushiDevices(current_dprice = false) {
        if (document.location.pathname == '/cart/purchase/') {
            if (current_dprice) {
                var devices_price = current_dprice;
                devices_price_ = Number(devices_price);
            }
        }
    }

    function recalcDevicesPrice() {
        $.ajax({
            type: "POST",
            url: "/cart/devices",
            data: {
                cart: true
            },
            async: false,
            success: function (request) {
                request_recalc = JSON.parse(request);
                for (var id in request_recalc) {
                    $('ul.tools-list span.purchase-offers_count--digit[data-id=' + id + ']').text(request_recalc[id]['count']);
                }

                basketSum -= devices_price_;
                $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
                $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
                changeBar(priceCountCnt);
                devices_price_ = 0;
            },
            error: function () {
                return false;
            }
        });
    }

    function getGift() {
        if (basketSum > 0) {
            if (document.location.pathname == '/cart/purchase/') {
                var gift_offer = 1;
                $('li.gift_offer').remove();
            } else {
                var gift_offer = 0;
            }
            $('li.gift_basket').remove();
            $.ajax({
                type: "POST",
                url: '/cart/gift',
                data: {
                    gift_offer: gift_offer
                },
                async: false,
                success: function (msg) {
                    if (msg != 'undefined') {
                        var request_array = JSON.parse(msg);
                        if (request_array) {
                            $('ul.basket-prods_list').append(request_array['gift_basket']);
                            if (request_array['gift_offer']) {
                                $('ul.purchase-offers_list').append(request_array['gift_offer']);
                            }
                        }
                    }
                }
            });

        }
    }

    function SelectSaveCompositeItems() {
        if ($('span.chosen_composite_food').length > 0) {
            var str_chosen = $('span.chosen_composite_food:last').data('chosen');
            var arr_chosen = str_chosen.split(',');
            if (arr_chosen.length > 0) {
                for (var i in arr_chosen) {
                    $('ul.drinks-list li[data-id=' + arr_chosen[i] + ']').addClass('btn_composite_item_add--select');
                }
                choose_composite_count = arr_chosen.length;
            } else {
                choose_composite_count = 0;
            }
        }
    }

    function getContextRules_(type_view = 2) {
        if (document.location.pathname == '/cart/purchase/') {
            type_view = 1;
            $('ul.recommends_list').html('');
            $('div.recommends').hide();
        }
        $('div.basket-offer').remove();
        $.ajax({
            type: "POST",
            url: "/context_rules/search",
            data: {
                type_view: type_view
            },
            async: false,
            success: function (request) {
                var request_parse = JSON.parse(request);
                if (request_parse) {
                    if (type_view == 2 && request_parse['view_2']) {
                        $('div.basket-modal').prepend(request_parse['view_2']);
                    } else if (type_view == 1) {
                        if (request_parse['view_1']) {
                            $('div.recommends').show();
                            $('ul.recommends_list').prepend(request_parse['view_1']);
                        }
                        if (request_parse['view_2']) {
                            $('div.basket-modal').prepend(request_parse['view_2']);
                        }
                    }
                }
            },
            error: function () {
                return false;
            }
        });
    }

    function selfRandom(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    getContextRules_();
    getGift();

    $('.sort-option').on('click', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        console.log(id);
        if (id) {
            $.ajax({
                type: "POST",
                url: "/menu_category/sort",
                data: {
                    sort_option_id: id
                },
                async: false,
                success: function (request) {
                    var request_json = JSON.parse(request);
                    if (request_json) {
                        location.reload();
                    }
                }
            });
        }
    });

    $(window).resize(function () {
        var height = Number($(this).height());
        if (height < 600) {
            console.log(height);
            $('.offer-bar').hide();
        } else {
            if (Number($(this).width()) > 1169) {
                $('.offer-bar').show();
            }
        }
    });
 
    if ($('#modal-w').find('.not_working_time').first().data('enabled') == 1) {

        $('a[href="#modal-w"]').trigger('click');
    }
    const modificatorList = document.getElementsByClassName('products-list_modificator');
  
    const getModificatorFood = function (event) {
        var xhr = new XMLHttpRequest();
        const params = "id=" + this.dataset.id;
        const parentId = this.dataset.parent;
        xhr.open('POST', '/shop/get-product', false);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(params);
        if (xhr.status != 200) {
            console.log(xhr.status + ': ' + xhr.statusText);
        } else {
            var data = JSON.parse(xhr.responseText);
            updateFood(parentId, data);
        }
        return false;
    }
    const updateFood = function(parentId, data){
        var food = document.querySelector(".product-item__item[data-id='" + parentId + "']");
        var image = food.querySelector('.lazy');
        image.setAttribute('src', "/" + data.MiniImageUrl);
        var priceContainer = food.querySelector('.product-item__price');
        priceContainer.innerHTML = data.Price + " ₽";
        var descriptionContainer = food.querySelector('.product-item__components');
        descriptionContainer.innerHTML = data.MiniComment;
        var weightContainer = food.querySelector('.product-item__portion');
        weightContainer.innerHTML = data.Weight;
        var button = food.querySelector('.product-item__btn');
        button.dataset.id = data.id;
        button.dataset.price = data.Price;
    }
    Array.from(modificatorList).forEach(function (modificator) {
        modificator.addEventListener('click', getModificatorFood);
    });

    // открытие инфо товара
    $('.product-popup').on('click', function() {
        if(document.documentElement.clientWidth > 577) {
            // на десктопе
            $.magnificPopup.open({
                showCloseBtn: true,
                removalDelay: 300,
                mainClass: 'mfp-fade',
                items: {
                    src: $('#modal-product'),
                },
                type: 'inline',
                callbacks: {
                    open: function() {
                        setPositionBgModificator($('#modal-product input:checked'));
                    }
                }
            });
        } else {
            // на мобилке
            $(this).closest('.product-item').siblings('.product-item_opened').removeClass('product-item_opened');
            $(this).closest('.product-item').toggleClass('product-item_opened');
        }
    });
});
// тормоз
function debounce(f, ms) {
	var isCooldown = false;

	return function() {
		if (isCooldown) return;
		f.apply(this, arguments);
		isCooldown = true;
		setTimeout(function() {isCooldown = false}, ms);
	};
}

function setPositionBgModificator($modInput) {
	var $modificator = $modInput.parent();
	var $modificatorBackground = $modificator.siblings('.product-item__modificator-background')
	var $positionModificator = $modificator.position();
	var $sizeModificator = {
		width: $modificator.width(),
		height: $modificator.height()
    };

	$modificatorBackground.css({
		top: $positionModificator.top,
		left: $positionModificator.left,
		width: $sizeModificator.width,
		height: $sizeModificator.height
	});
}

function allSetPositionBgModificator() {
	var modificators = $('.product-item__modificator input:checked');

	modificators.each(function(i, mod) {
        setPositionBgModificator($(mod));
        mod.click();
	});
}

var debAllSetPositionBgModificator = debounce(allSetPositionBgModificator, 150);

$(window).on('load', allSetPositionBgModificator);
$(window).on('resize', debAllSetPositionBgModificator);

$(document).on('change', '.product-item__modificator input', function(e) {
	e.preventDefault();

	setPositionBgModificator($(this));
});