var API_YANDEX_MAP = 'https://api-maps.yandex.ru/2.1/?apikey=d4522b54-9a89-4678-a7ce-b8add3933265&lang=ru_RU';
var YANDEX_MAP = 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A9b8f1c7e9f396a2e43fb46fcf919ffc3b4f894bbfbfcc143076b88a91bcf48d3&amp;width=100%&amp;height=540&amp;lang=ru_RU&amp;scroll=true';

// тормоз
function debounce(f, ms) {
    var isCooldown = false;

    return function () {
        if (isCooldown) return;
        f.apply(this, arguments);
        isCooldown = true;
        setTimeout(function () {
            isCooldown = false
        }, ms);
    };
}

// рассчет расстояния
function calculateDistance(coordinates) {
    var EARTH_RADIUS = 6372795;

    // перевести координаты в радианы
    var lat1 = coordinates.lat1 * Math.PI / 180;
    var lon1 = coordinates.lon1 * Math.PI / 180;
    console.log(coordinates.lat1);
    console.log(coordinates.lon1);
    var affiliates = coordinates.affiliates;
    var distance = 0;
    var key = null;
    Object.keys(affiliates).forEach(function (k) {
        affiliate = JSON.parse(affiliates[k]);
        var lat2 = parseFloat(affiliate.lat) * Math.PI / 180;
        var lon2 = parseFloat(affiliate.long) * Math.PI / 180;

        // косинусы и синусы широт и разницы долгот
        var cosLat1 = Math.cos(lat1);
        var cosLat2 = Math.cos(lat2);
        var sinLat1 = Math.sin(lat1);
        var sinLat2 = Math.sin(lat2);
        var delta = lon2 - lon1;
        var cosDelta = Math.cos(delta);
        var sinDelta = Math.sin(delta);

        // вычисления длины большого круга
        var y = Math.sqrt(Math.pow(cosLat2 * sinDelta, 2) + Math.pow(cosLat1 * sinLat2 - sinLat1 * cosLat2 * cosDelta, 2));
        var x = sinLat1 * sinLat2 + cosLat1 * cosLat2 * cosDelta;

        var ad = Math.atan2(y, x);

        var new_distance =  Math.round(ad * EARTH_RADIUS / 100) / 10;
        console.log(affiliate.lat + '>>>' + affiliate.long + '>>>' + new_distance + " >>> " + k);
        if(distance == 0 || new_distance < distance){
            distance = new_distance;
            key = k;
        }
    });
    return affiliates[key];
}

// получение от яндекса адресов
function getAddresses(value) {
    return new Promise(function (resolve, reject) {
        var geocoder = ymaps.geocode('Россия Красноярск ' + value, {
            results: 8
        });

        geocoder.then(function (res) {
            var items = [];

            res.geoObjects.each(function (obj) {
                var coors = obj.geometry.getCoordinates();

                items.push({
                    name: obj.properties.get('description') + ', ' + obj.properties.get('name'),
                    lat: coors[0],
                    lon: coors[1]
                });
            });

            resolve(items);
        });
    });
};

// добавление адресов в верстку
function setListAdresses() {
    var input = $(this);
    var value = input.val();
    var resultContainer = input.siblings('.search-field__result');
    var htmlItems = '';

    if (value.length < 3) {
        resultContainer.html('').css('display', 'none');
        input.parent().removeClass('valid-address');
        return;
    }

    ymaps.ready(function () {
        getAddresses(value).then(function (items) {

            if (resultContainer.find('li:first-of-type a').text().trim() == items[0].name && items.length === 1) {
                resultContainer.html('').css('display', 'none');
                return;
            }

            for (var i = 0; i < items.length; i++) {
                var item = items[i];

                htmlItems += ['<li><a data-lat="', item.lat, '" data-lon="', item.lon, '" href="javascript:void(0);">', item.name, '</a></li>'].join('');
            }
            resultContainer.html(htmlItems).css('display', 'block');
        });
    });
}

var debSetListAdresses = debounce(setListAdresses, '250');

$(document).ready(function () {
    // открытие модалки на рассчет времени
    $('a[href="#get-delivery-time"]').magnificPopup({
        type: 'inline',
        mainClass: 'cn-modal',
        closeMarkup: '<button title="%title%" type="button" class="mfp-close"><i class="icon-close"></i></button>',
        callbacks: {
            beforeOpen: function () {
                var mapElement = $('#delivery-time-modal-map');

                if (!$('#api-maps-yandex').length) {
                    $('body').append('<script id="api-maps-yandex" src="' + API_YANDEX_MAP + '"></script>');
                }

                if (!mapElement.html().length) {
                    mapElement.html('<script src="' + YANDEX_MAP + '"></script>');
                }
            },
            afterClose: function () {
                var input = $('#search-address');
                var selectAdress = $('.delivery-time-modal-select-address');
                var deliveryTimeResult = $('.delivery-time-modal-result');

                input.val('').parent().removeClass('valid-address');
                selectAdress.css('display', 'flex');
                deliveryTimeResult.css('display', 'none');
            },
        }
    });

    // ввод адреса
    $('#search-address').on('input', debSetListAdresses);

    $('#search-address ~ .search-field__result').on('click', 'a', function (e) {
        e.preventDefault();

        var input = $('#search-address');
        var lat = $(this).data('lat');
        var long = $(this).data('lon');
        $('#search-address').data('lat', lat);
        $('#search-address').data('long', long);
        input.val($(this).text() + ' ').focus();
        input.parent().addClass('valid-address');
        input.trigger('input');
    });

    // возврат к адресу в доставке
    $('#refresh-address').on('click', function (e) {
        e.preventDefault();

        var selectAddress = $('.delivery-time-modal-select-address');
        var deliveryTimeResult = $('.delivery-time-modal-result');
        var resultContainer = $('#search-address ~ .search-field__result');

        selectAddress.css('display', 'flex');
        selectAddress.find('input').val('');

        deliveryTimeResult.css('display', 'none');

        $('#search-address').parent().removeClass('valid-address');

        resultContainer.html('').css('display', 'none');
    });
});