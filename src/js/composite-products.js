import { getCompositeProducts, setProduct } from './api/product';
import { random } from './helper';

let selectedProducts = [];
let compositeProductId;
let compositeId;
let countProductInComposite;

function getHtmlProductInComposite(product) {
	return `
		<li>
			<a class="product-item" href="javascript:void(0);" aria-selected="false" data-product-id="${product.id}">
				<i class="icon-svg-check"></i>
				<div class="product-item__img">
					<img src="/${product.ImageUrl}">
				</div>
				<div class="product-item__content">
					<span class="product-item__name">${product.Name}</span>
					${product.MiniComment ? '<span class="product-item__components">' + product.MiniComment + '</span>' : ''}
				</div>
			</a>
		</li>`;
}

// открытие модального окна
$('.product-popup').on('click', function() {
	if(!$(this).closest('.product-item_composite').length) return;

	const $composite = $(this).closest('.product-item_composite');
	const productIdsInComposite = $composite.data('composite-foods');

	compositeProductId = $composite.data('product-id');
	compositeId = random(1000000, 2000000);
	countProductInComposite = $composite.data('composite-count');

	getCompositeProducts(compositeProductId, compositeId, productIdsInComposite).then((r) => {
		const items = JSON.parse(r);
		const compositeList = [];

		for (const item of items) {
			compositeList.push(getHtmlProductInComposite(item));
		}

		return `
			<div class="cb-modal modal-composite-product" id="modal-composite-product">
				<div class="cb-modal-wrap">
					<div class="h1 modal-composite-product__title">ДОБАВЬТЕ БЛЮДА В НАБОР <span class="color-red">0/${countProductInComposite}</span></div>
					<div class="modal-composite-product__list-wrap">
						<ul class="ul modal-composite-product__list">
							${compositeList.join('')}
						</ul>
					</div>
					<button class="btn btn_type_gradient-red modal-composite-product__submit full-width"><span>Добавить в корзину</span></button>
				</div>
			</div>`;

	}).then((modal) => {
		$.magnificPopup.open({
			items: {
				src: modal,
				type: 'inline',
			},
			callbacks: {
				close: function() {
					selectedProducts = [];
					$('.modal-composite-product .product-item[aria-selected="true"]').each(function() {
						$(this).attr('aria-selected', 'false');
					});
					$('.modal-composite-product__title span').addClass('color-red').removeClass('color-green').text('0/4');
				}
			}
		}, 0);
	})
});

// выбор товаров
document.addEventListener('click', (e) => {
	if(!e.target.closest('.modal-composite-product .product-item')) return;

	const productEl = e.target.closest('.modal-composite-product .product-item');
	const countSelectedEl = document.querySelector('.modal-composite-product__title span');	

	// проверяем выбран ли продукт
	if (productEl.getAttribute('aria-selected') === 'false') {
		// если уже 4 выбрано, удаляем первый выбранный
		if (selectedProducts.length === countProductInComposite) {
			const firstSelected = selectedProducts.shift();
			document.querySelector('.modal-composite-product .product-item[data-product-id="'+firstSelected+'"]').setAttribute('aria-selected', 'false');
		}
		// добавляем новый
		productEl.setAttribute('aria-selected', 'true');
		selectedProducts.push(productEl.dataset.productId);
	} else {
		// удаляем выбранный
		productEl.setAttribute('aria-selected', 'false');
		selectedProducts.splice(selectedProducts.indexOf(productEl.dataset.productId), 1);
	}

	countSelectedEl.innerHTML = selectedProducts.length + '/' + countProductInComposite;

	if(selectedProducts.length === countProductInComposite) {
		countSelectedEl.classList.add('color-green');
		countSelectedEl.classList.remove('color-red');
	} else {
		countSelectedEl.classList.add('color-red');
		countSelectedEl.classList.remove('color-green');
	}
});

// заказ выбранных ролов в наборе
document.addEventListener('click', (e) => {
	if(!e.target.closest('.modal-composite-product__submit')) return;
	if(selectedProducts.length != countProductInComposite) return;

	setProduct(compositeProductId, 1, { foods_id_str: selectedProducts.join(','), composite_id: compositeId })
		.then(() => {
			$.magnificPopup.close();
		});
});