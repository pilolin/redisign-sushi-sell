import { getProduct } from './api/product';
import { getCart } from './api/general';

$(document).ready(function () {
    $(".type-delivery-link").click(function () {
        const data = $(this).attr('data-yourself');
        if (data == 2) {
            $('#deliveryform-type_yourself').attr('value', 2);
            $('.cities_tab, .address-form2').hide();
            if ($('a[data-order_type=2]').hasClass('pay-tab_link--active')) {
                $('.tab-pickups').hide();
                $('.tab-pickups').removeClass('tab-content--active');
            } else if ($('a[data-order_type=1]').hasClass('pay-tab_link--active')) {
                $('.tab-pickups').show();
                $('.tab-pickups').add('tab-content--active');
            }
            $('.discount_block').show();
        }
        if (data == 1) {
            $('#deliveryform-type_yourself').attr('value', 1);
            $('.discount_block').hide();
            if ($('a[data-order_type=2]').hasClass('pay-tab_link--active')) {
                $('.cities_tab, .address-form2').hide();
                $('.tab-pickups').hide();
                $('.tab-pickups').removeClass('tab-content--active');
            } else if ($('a[data-order_type=1]').hasClass('pay-tab_link--active')) {
                $('.cities_tab, .address-form2').show();
                $('.tab-pickups').hide();
                $('.tab-pickups').removeClass('tab-content--active');
            }
        }
    });

    $('.take-list_choose').click(function () {
        $('.take-list_choose').removeClass('take_list_choose_active');
        $(this).addClass('take_list_choose_active');
        var pickup_id = $(this).data('id');
        $('#deliveryform-affiliate_id').attr('value', pickup_id);

        return false;
    });

    $('.cities_tab a.pay-tab_link').on('click', function () {
        var city_id = $(this).attr('data-city_id');
        if (city_id) {
            $('#deliveryform-city').attr('value', city_id);
        }
    });

    if ($(window).innerWidth() < 1170) {
        $('.header-bottom_basket').on('click', function (e) {
            e.preventDefault();
            $(this).parent().toggleClass('mobile-cart');
        });
    };

    const itemCount = $('.basket-icon_num');

    $('.purchase-popup').on('click', function () {
        var attr = $(this).attr('data-url'),
            title = $(this).parent().find('.product-item__name'),
            price = $(this).parent().find('.product-item__price'),
            descr = $(this).parent().find('.product-full-comment'),
            portion = $(this).parent().find('.product-item__portion'),
            dataPrice = $(this).find('.product-item__btn').attr('data-price');

        $('.product-item__img--modal img').attr("src", attr);
        $('.product-item__name--modal').text(title.text());
        $('.product-item__price--modal').html(price.html());
        $('.product-item__components--modal').text(descr.text());
        $('.product-item__portion--modal').html(portion.html());
        $('.product-item__btn--modal').attr('data-price', dataPrice);
    });

    var basketCnt = 0;
    var basketSum = Number($('.basket-prods_total').data('sum'));


    function enableClick() {
        $('.basket-prods_cnt').css("pointer-events", "auto");
    }

    $('ul.basket-prods_list').on('click', '.basket-prods_cnt--plus', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var composite = Number($(this).parent().parent().attr('data-iscomposite'));
        if ((composite == 1) && (document.location.pathname != '/cart/purchase/')) {
            $('a.composite_modal_link_basket[data-id=' + id + ']').click();
            return;
        }
        basketCnt = parseInt($(this).parent().find('.basket-prods_num').text());
        $(this).parent().find('.basket-prods_num').text(++basketCnt);
        $('span.purchase-offers_count--digit[data-id=' + id + ']').text(basketCnt);
        var price = Number($(this).parent().parent().find('.basket-prods_price').text().trim());
        basketSum += price;
        $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
        $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
        $('.purchase-offers_count--digit[data-id=' + id + ']').text(basketCnt);
        changeOfferBar(priceCountCnt);
        $('.basket-prods_cnt').css("pointer-events", "none");
        setTimeout(enableClick, 1000);
        getContextRules_();
        recalcDevicesPrice();
    });

    $('ul.basket-prods_list').on('click', '.basket-prods_cnt--minus', function (e) {
        e.preventDefault();
        basketCnt = parseInt($(this).parent().find('.basket-prods_num').text());
        if (basketCnt == 0) {
            return;
        }

        var id = $(this).data('id');
        var composite = Number($(this).parent().parent().attr('data-iscomposite'));
        basketCnt = $(this).parent().find('.basket-prods_num').text();
        $(this).parent().find('.basket-prods_num').text(--basketCnt);
        $('span.purchase-offers_count--digit[data-id=' + id + ']').text(basketCnt);
        var price = Number($(this).parent().parent().find('.basket-prods_price').text().trim());
        basketSum -= price;
        $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
        $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
        $('.purchase-offers_count--digit[data-id=' + id + ']').text(basketCnt);
        var food_count = Number($(this).parent().find('.basket-prods_num').text().trim());
        $('.basket-prods_cnt').css("pointer-events", "none");
        setTimeout(enableClick, 1000);
        changeOfferBar(priceCountCnt);
        checkSubmit();
        if (food_count == 0) {
            $(this).parent().parent().remove();
            $('.purchase-offers_item[data-id=' + id + ']').remove();
        }
        getContextRules_();
        recalcDevicesPrice();
    });

    var priceCountCnt = +Number($('.basket-icon_digits').text());


    $('.basket-prods_cnt').on('click', function (e) {
        e.preventDefault();
        $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
    });

    $('ul.basket-prods_list').on('click', '.basket-prods_close', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var price = Number($(this).parent().find('.basket-prods_price').text().trim());
        var food_count = Number($(this).parent().find('.basket-prods_num').text().trim());
        var request;
        $.ajax({
            type: "POST",
            url: "/cart/del",
            data: {
                id: id
            },
            async: false,
            success: function (msg) {
                if (msg != 'undefined') {
                    $('li.gift_basket').remove();
                    $('li.gift_offer').remove();
                    var request = JSON.parse(msg);
                    if (request) {
                        $('ul.basket-prods_list').append(request['gift_basket']);
                    }
                }
            }
        })
        if (request == 'undefined') {
            return false;
        }
        basketSum -= food_count * price;
        $(this).parent('.basket-prods_item').remove();
        changeOfferBar(priceCountCnt);
        checkSubmit();
        $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
        $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
        $('.purchase-offers_item[data-id=' + id + ']').remove();
        getContextRules_();
        recalcDevicesPrice();
        $('a.composite_food_add[data-id=' + id + ']').parent().parent().remove();
    });

    $('ul.purchase-offers_list').on('click', '.purchase-offers_count--plus', function () {
        var id = $(this).data('id');
        var composite = Number($(this).parent().parent().parent().attr('data-iscomposite'));
        var price = Number($(this).data('price'));
        basketCnt = parseInt($('li.basket-prods_item[data-id=' + id + ']').find('span.basket-prods_num').text());
        $('li.basket-prods_item[data-id=' + id + '] span.basket-prods_num').text(++basketCnt);
        basketSum += price;
        $(this).parent().find('.purchase-offers_count--digit').text(basketCnt);
        $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
        $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
        changeOfferBar(priceCountCnt);
        $('.basket-prods_cnt').css("pointer-events", "none");
        setTimeout(enableClick, 1000);
        getContextRules_();
        recalcDevicesPrice();
    });

    $('ul.purchase-offers_list').on('click', '.purchase-offers_count--minus', function () {
        var id = $(this).data('id');
        var composite = Number($(this).parent().parent().parent().attr('data-iscomposite'));
        var price = Number($(this).data('price'));
        basketCnt = parseInt($('li.basket-prods_item[data-id=' + id + ']').find('span.basket-prods_num').text());
        $('li.basket-prods_item[data-id=' + id + '] span.basket-prods_num').text(--basketCnt);
        basketSum -= price;
        $(this).parent().find('.purchase-offers_count--digit').text(basketCnt);
        $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
        $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
        var food_count = Number($(this).parent().find('.purchase-offers_count--digit').text().trim());
        changeOfferBar(priceCountCnt);
        checkSubmit();
        if (food_count == 0) {
            $('.purchase-offers_item[data-id=' + id + ']').remove();
            $('.basket-prods_item[data-id=' + id + ']').remove();
        }
        $('.basket-prods_cnt').css("pointer-events", "none");
        setTimeout(enableClick, 1000);
        getContextRules_();
        recalcDevicesPrice();
    });

    $('ul.tools-list').on('click', '.purchase-offers_count--plus', function () {
        var id = $(this).data('id');
        var dataPrice = $(this).data('price');
        var qty = Number($(this).parent().find('.purchase-offers_count--digit').text());
        $.ajax({
            type: "POST",
            url: '/cart/change_devices_count',
            data: {
                id: id,
                action_: 'plus'
            },
            async: false,
            success: function (request) {
                request_price = JSON.parse(request);
                devices_price_ = request_price;
            }
        });
        if (basketSum >= 0) {
            basketSum += dataPrice;
            $(this).parent().find('.purchase-offers_count--digit').text(++qty);
            $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
            $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
            changeOfferBar(priceCountCnt);
        }
    });

    $('ul.tools-list').on('click', '.purchase-offers_count--minus', function () {
        var id = $(this).data('id');
        var dataPrice = $(this).data('price');
        var qty = Number($(this).parent().find('.purchase-offers_count--digit').text());
        var th = $(this);
        $.ajax({
            type: "POST",
            url: '/cart/change_devices_count',
            data: {
                id: id,
                action_: 'minus'
            },
            async: false,
            success: function (request) {
                request_price = JSON.parse(request);
                if (request_price['qty'] <= qty - 1) {
                    devices_price_ = request_price['d_price'];
                    if (basketSum > 0) {
                        basketSum -= dataPrice;
                        th.parent().find('.purchase-offers_count--digit').text(--qty);
                        $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
                        $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
                        changeOfferBar(priceCountCnt);
                    }
                }
            }
        });
    })

    $(document).on('click', '.purchase-btn', function () {

        var id = $(this).data('id');
        var composite = Number($(this).parent().parent().attr('data-iscomposite'));
        var isset = false;
        $('ul.basket-prods_list li').each(function (index, element) {
            var current_id = $(this).attr('data-id');
            if (current_id == id) {
                isset = true;
            }
        });
        if (isset == true) {
            $('span.basket-prods_cnt--plus[data-id=' + id + ']').click();
        } else {
            var request;
            $.ajax({
                type: "POST",
                url: "/cart/add",
                data: {
                    id: id,
                    type_render: 2
                },
                async: false,
                success: function (request) {
                    if (request != 'undefined') {
                        var request_array = JSON.parse(request);
                        $('ul.basket-prods_list').prepend(request_array['row1']);
                        $('ul.purchase-offers_list').prepend(request_array['row2']);
                        $('li.gift_basket').remove();
                        $('li.gift_offer').remove();
                        if (request_array['gift_basket']) {
                            $('ul.basket-prods_list').append(request_array['gift_basket']);
                            if (request_array['gift_offer']) {
                                $('ul.purchase-offers_list').append(request_array['gift_offer']);
                            }
                        }
                    }
                }
            });
        }

        var _this = $(this),
            elBasket = $('.header-bottom_basket'),
            elBasketOffset = elBasket.offset();
        if ($(window).scrollTop() > HeaderTop) {
            elBasketOffset = {
                top: elBasket.offset().top - 90,
                left: elBasket.offset().left
            };
        }
        parent = _this.closest('li'),
            img = parent.find('.product-item__img'),
            imgOffset = img.offset(),
            imgClone = img.clone().addClass('animate').css({
                position: 'absolute',
                zIndex: 9999,
                top: imgOffset.top + "px",
                left: imgOffset.left + "px",
                width: img.width()
            });

        var price = +$(this).attr('data-price');

        $('body').append(imgClone).find('.product-item__img.animate').animate({
            top: elBasketOffset.top + (elBasket.height() / 2),
            left: elBasketOffset.left + (elBasket.width() / 2),
            height: "hide",
            width: "hide",
        }, {
            duration: 300,
            complete: function () {
                $(this).remove();
            }
        });

        if (isset == false) {
            changeOfferBar(priceCountCnt);
            basketSum += price;
            $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
            $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
            checkSubmit();
            getContextRules_();
            recalcDevicesPrice();
        }

        return false;
    });

    $('a.purchase-form_btn').on('click', function (e) {
        e.preventDefault();
        var current_obj = $(this);
        $('.composite_alert_text').remove();
        $('li.purchase-offers_item').removeClass('composite_food_alert');

        $.ajax({
            type: "POST",
            url: '/cart/testcompositefoods',
            async: false,
            success: function (msg) {
                if (msg == 1) {
                    window.location = '/cart/delivery/';
                } else {
                    var request_array = JSON.parse(msg);
                    if (!($.isEmptyObject(request_array))) {
                        var chosen_composite_foods = [];
                        for (var i in request_array) {
                            for (var j in request_array[i]) {
                                chosen_composite_foods.push(j);
                            }
                        }
                        var show_alert_message = false;
                        $('.composite_food_add').each(function () {
                            var composite_id = $(this).attr('data-composite_id');
                            if (chosen_composite_foods.indexOf(composite_id) == -1) {
                                show_alert_message = true;
                                $(this).parent().parent().addClass('composite_food_alert');
                            }
                        });
                        if (show_alert_message) {
                            current_obj.parent().parent().append("<h2 class='composite_alert_text'>Укажите состав у всех наборов!</h2>");
                        }
                    }
                }
            }
        });

    });

    $('.pay-tab_link').on('click', function (e) {
        e.preventDefault();
        var pay_type = $(this).data('pay_type');
        $('#payform-pay_type').attr('value', pay_type);
        $(this)
            .addClass('pay-tab_link--active')
            .parent().siblings().find('.pay-tab_link')
            .removeClass("pay-tab_link--active")
            .closest(".pay-wrap")
            .find(".pay-total")
            .removeClass("pay-total--active")
            .eq($(this).parent().index())
            .addClass("pay-total--active");
    });

    $('.submit-tab_link').on('click', function (e) {
        e.preventDefault();
        $(this)
            .addClass('pay-tab_link--active')
            .parent().siblings().find('.submit-tab_link')
            .removeClass("pay-tab_link--active")
            .closest(".submit")
            .find(".tab-content")
            .removeClass("tab-content--active")
            .eq($(this).parent().index())
            .addClass("tab-content--active");
    });

    $(document).on('click', '.recommends_item--add', function () {
        var id = $(this).attr('data-id');
        $("a.purchase-offers_add[data-id='" + id + "']").click();
    });

    $('ul.product-item button.product-item__btn, .banner-list-btn-food').on('click', function () {
        var id = $(this).attr('data-id');
        if (Number($(this).attr('data-iscomposite')) == 1 && Number($(this).attr('data-composite_count')) > 1) {
            $('a.composite_modal_link[data-id=' + id + ']').click();
            return;
        }
        var isset = false;
        $(document).find('li.basket-prods_item').each(function (index, element) {
            var current_id = $(element).attr('data-id');
            if (current_id == id) {
                isset = true;
            }
        });
        if (isset == true) {
            $('span.basket-prods_cnt--plus[data-id=' + id + ']').click();
        } else {
            var price = $(this).parent().find('.product-item__price');
            cat_id = $(this).data('cat');
            $('button.product-item__btn').prop("disabled", true);
            $.ajax({
                type: "POST",
                url: "/cart/add",
                data: {
                    id: id,
                    type_render: 1
                },
                async: false,
                success: function (msg) {
                    setTimeout(function(){
                        $('button.product-item__btn').prop("disabled", false);
                    }, 500);
                    if (msg != 'undefined') {
                        var request_array = JSON.parse(msg);
                        $('li.gift_basket').remove();
                        $('ul.basket-prods_list').prepend(request_array['row1']);
                        if (request_array['gift_basket']) {
                            $('ul.basket-prods_list').append(request_array['gift_basket']);
                        }
                    }
                }
            });
        }
        var _this = $(this),
            elBasket = $('.header-bottom_basket'),
            elBasketOffset = elBasket.offset();
        if ($(window).scrollTop() > HeaderTop) {
            elBasketOffset = {
                top: elBasket.offset().top - 90,
                left: elBasket.offset().left
            };
        }
        var parent = _this.closest('li');
        if (_this.parent().parent().hasClass('products-list_item--action')) {
            var img = _this.parent().parent().find('.product-item__img');
            img.show();
        } else {
            var img = parent.find('.product-item__img');
        }
        var imgOffset = img.offset();
        var imgClone = img.clone().addClass('animate').css({
            position: 'absolute',
            zIndex: 9999,
            top: imgOffset.top + "px",
            left: imgOffset.left + "px",
            width: img.width()
        });

        var price = +$(this).attr('data-price');

        $('body').append(imgClone).find('.product-item__img.animate').animate({
            top: elBasketOffset.top + (elBasket.height() / 2),
            left: elBasketOffset.left + (elBasket.width() / 2),
            height: "hide",
            width: "hide",
        }, {
            duration: 300,
            complete: function () {
                $(this).remove();
            }
        });
        if (isset == false) {
            changeOfferBar(priceCountCnt);
            basketSum += price;
            $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
            checkSubmit();
            getContextRules_();
        }
        if ($(this).parent().parent().hasClass('products-list_item--action')) {
            $(this).parent().parent().remove();
        }
        return false;
    });

    var HeaderTop = $('header').innerHeight();


    $('.magnific-popup').on('click', function() {
        $.magnificPopup.open({
            items: {
                src: $(this).attr('href'),
                type: 'inline',
            },
            preloader: false,
            modal: true
        });
    });

    $('.footer-policy_link').on('click', function() {
        $.magnificPopup.open({
            items: {
                src: $(this).attr('href'),
                type: 'inline',
            },
            preloader: false,
            modal: true
        });
    });

    $('.policy_link').on('click', function() {
        $.magnificPopup.open({
            items: {
                src: $(this).attr('href'),
                type: 'inline',
            },
            preloader: false,
            modal: true
        });
    });

    $('.header-contact_callback').on('click', function() {
        $.magnificPopup.open({
            items: {
                src: $(this).attr('href'),
                type: 'inline',
            },
            preloader: false,
            modal: true
        });
    });

    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    if ($(window).width() > 768) {};


    $('.js-submit').submit(function (e) {
        e.preventDefault();
        var phone = $(this).find('input[name="phone"]');
        if (phone.val() == "") {
            phone.focus();
            return false;
        } else {
            var form_data = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "/sendmessage.php",
                data: form_data,
                async: false,
                success: function () {}
            });
        }
        return false;
    });

    $('.basket-modal').on('click', '.offer-item_submit', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var composite = Number($(this).attr('data-iscomposite'));

        var isset = false;
        $('ul.basket-prods_list li').each(function (index, element) {
            var current_id = $(this).attr('data-id');
            if (current_id == id) {
                isset = true;
            }
        });
        if (isset == true) {
            $('span.basket-prods_cnt--plus[data-id=' + id + ']').click();
        } else {
            var request;
            $.ajax({
                type: "POST",
                url: "/cart/add",
                data: {
                    id: id,
                    type_render: 2
                },
                async: false,
                success: function (request) {
                    if (request != 'undefined') {
                        var request_array = JSON.parse(request);
                        $('ul.basket-prods_list').append(request_array['row1']);
                        $('ul.purchase-offers_list').prepend(request_array['row2']);
                        $('li.gift_basket').remove();
                        $('li.gift_offer').remove();
                        if (request_array['gift_basket']) {
                            $('ul.basket-prods_list').append(request_array['gift_basket']);
                            if (request_array['gift_offer']) {
                                $('ul.purchase-offers_list').append(request_array['gift_offer']);
                            }
                        }
                    }
                }
            });
        }

        var _this = $(this),
            elBasket = $('.header-bottom_basket'),
            elBasketOffset = elBasket.offset();
        if ($(window).scrollTop() > HeaderTop) {
            elBasketOffset = {
                top: elBasket.offset().top - 90,
                left: elBasket.offset().left
            };
        }
        parent = _this.closest('.offer-item'),
            img = parent.find('.offer-item_image'),
            imgOffset = img.offset(),
            imgClone = img.clone().addClass('animate').css({
                position: 'absolute',
                zIndex: 9999,
                top: imgOffset.top + "px",
                left: imgOffset.left + "px",
                width: img.width()
            });

        var price = +$(this).attr('data-price');

        $('body').append(imgClone).find('.offer-item_image.animate').animate({
            top: elBasketOffset.top + (elBasket.height() / 2),
            left: elBasketOffset.left + (elBasket.width() / 2),
            height: "hide",
            width: "hide",
        }, {
            duration: 300,
            complete: function () {
                $(this).remove();
            }
        });

        if (isset == false) {
            changeOfferBar(priceCountCnt);
            basketSum += price;
            $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
            $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
            checkSubmit();
            getContextRules_();
            recalcDevicesPrice();
        }
        return false;
    });

    if($.fn.owlCarousel) {
        $('.main-slider').owlCarousel({
            loop: true,
            responsive: {
                0: {
                    items: 1,
                    mouseDrag: false,
                    touchDrag: true
                },
            },
            items: 1,
            nav: false,
            navText: '',
            autoplay: true,
            autoplayTimeout: 7000,
            autoplayHoverPause: true,
            dots: true,
        });
    }


    $('a[data-order_type=2]').on('click', function (e) {
        e.preventDefault();
        $('div.vacancy-form_left').css({
            'display': 'inline-block'
        });
        $('.cities_tab, .address-form2').hide();
        $('#deliveryform-order_type').attr('value', 2);
        $('div.delivery-time_content').hide();
        $('div.tab-pickups').removeClass('tab-content--active');
        if ($('a[data-yourself=2]').hasClass('pay-tab_link--active')) {
            $('div.tab-pickups').hide();
            $('.tab-pickups').removeClass('tab-content--active');
            $('.discount_block').show();
        } else {
            $('.tab-pickups').removeClass('tab-content--active');
            $('.discount_block').hide();
        }
    });

    $('a[data-order_type=1]').on('click', function (e) {
        e.preventDefault();
        $('#phone_block').removeAttr('id');
        $('#name_block').removeAttr('id');
        $('div.submit div.submit-1st').show();
        $('div.delivery-time_content').show();
        $('div.vacancy-form_left').css({
            'display': '',
            'margin-left': '',
            'margin-right': ''
        });

        if ($('a[data-yourself=2]').hasClass('pay-tab_link--active')) {
            $('.cities_tab, .address-form2').hide();
            $('div.delivery-agreement').css({
                'display': 'block',
                'margin-left': 'auto',
                'margin-right': 'auto'
            });
            $('#deliveryform-order_type').attr('value', 1);
            $('div.tab-pickups').show();
            $('div.tab-pickups').addClass('tab-content--active');
            $('.discount_block').show();
        } else if ($('a[data-yourself=1]').hasClass('pay-tab_link--active')) {
            $('#deliveryform-order_type').attr('value', 1);
            $('.tab-pickups').hide();
            $('.tab-pickups').removeClass('tab-content--active');
            $('.cities_tab, .address-form2').show();
            $('.discount_block').hide();
        }

    });

    var devices_price_ = Number($('ul.basket-prods_list').data('devices'));
    if (devices_price_ > 0) {
        basketSum += devices_price_;
    }

    function recalcDevicesPrice() {
        $.ajax({
            type: "POST",
            url: "/cart/devices",
            data: {
                cart: true
            },
            async: false,
            success: function (request) {
                request_recalc = JSON.parse(request);
                for (var id in request_recalc) {
                    $('ul.tools-list span.purchase-offers_count--digit[data-id=' + id + ']').text(request_recalc[id]['count']);
                }

                basketSum -= devices_price_;
                $('.basket-prods_total').html('Итого: ' + basketSum + ' ' + '<i></i>');
                $('.cart-total_count').html(basketSum + ' ' + '<i></i>');
                changeOfferBar(priceCountCnt);
                devices_price_ = 0;
            },
            error: function () {
                return false;
            }
        });
    }

    function getGift() {
        if (basketSum > 0) {
            if (document.location.pathname == '/cart/purchase/') {
                var gift_offer = 1;
                $('li.gift_offer').remove();
            } else {
                var gift_offer = 0;
            }
            $('li.gift_basket').remove();
            $.ajax({
                type: "POST",
                url: '/cart/gift',
                data: {
                    gift_offer: gift_offer
                },
                async: false,
                success: function (msg) {
                    if (msg != 'undefined') {
                        var request_array = JSON.parse(msg);
                        if (request_array) {
                            $('ul.basket-prods_list').append(request_array['gift_basket']);
                            if (request_array['gift_offer']) {
                                $('ul.purchase-offers_list').append(request_array['gift_offer']);
                            }
                        }
                    }
                }
            });

        }
    }

    function getContextRules_(type_view = 2) {
        if (document.location.pathname == '/cart/purchase/') {
            type_view = 1;
            $('ul.recommends_list').html('');
            $('div.recommends').hide();
        }
        $('div.basket-offer').remove();
        $.ajax({
            type: "POST",
            url: "/context_rules/search",
            data: {
                type_view: type_view
            },
            async: false,
            success: function (request) {
                var request_parse = JSON.parse(request);
                if (request_parse) {
                    if (type_view == 2 && request_parse['view_2']) {
                        $('div.basket-modal').prepend(request_parse['view_2']);
                    } else if (type_view == 1) {
                        if (request_parse['view_1']) {
                            $('div.recommends').show();
                            $('ul.recommends_list').prepend(request_parse['view_1']);
                        }
                        if (request_parse['view_2']) {
                            $('div.basket-modal').prepend(request_parse['view_2']);
                        }
                    }
                }
            },
            error: function () {
                return false;
            }
        });
    }

    getContextRules_();
    getGift();

    const getModificatorFood = (e) => {
        getProduct(e.target.value).then((r) => {
            updateFood(r);
        })
    }
    const updateFood = function(data){
        const selectorProd = !document.querySelector('.cb-modal.modal-product') ? '.product-item[data-product-parent="' + data.mainFoodId + '"]' : '.cb-modal.modal-product[data-product-id]';
        const food = document.querySelector(selectorProd);

        food.querySelector('.product-item__img img').src = '/' + data.MiniImageUrl;
        food.querySelector('.product-item__price').innerHTML = `<span>${data.Price}</span><i class="icon-rub"></i>`;
        food.querySelector('.product-item__components').innerHTML = data.MiniComment;
        if(food.querySelector('.product-item__footer .product-item__prop')) {
            food.querySelector('.product-item__footer .product-item__prop').innerHTML = data.Weight;
        }
        food.dataset.productId = data.id;

        getCart().then((e) => {
            const inCart = Object.keys(e.items).indexOf(''+data.id) !== -1;

            food.classList[inCart ? 'add' : 'remove']('product-item_in-cart');
            food.querySelector('.form-item__number input').value = inCart ? e.items[data.id].qty : 0;
        });
    }


    document.addEventListener('change', (e) => {
        if (!e.target.closest('.product-item__modificator')) return;

        getModificatorFood(e);
    });
});