const offerBar = document.querySelector('.offer-bar');

if (offerBar) {
	const scale = offerBar.querySelector('.offer-bar-scale__current');
	const markers = offerBar.querySelectorAll('.offer-bar-scale__marker');

	function changeOfferBar(val) {
		if(val === undefined || isNaN(+val)) return;
	
		offerBar.dataset.current = +val;
	}
	
	function initOfferBar() {
		const max = +offerBar.dataset.max;
		const current = (+offerBar.dataset.current > max) ? max : +offerBar.dataset.current;
	
		scale.style.width = (current / max * 100) + '%';
	
		if (current <= 0) {
			setTimeout(() => {
				scale.style.opacity = 0;
			}, 320);
		} else {
			scale.style.opacity = 1;
		}
	
		for (const marker of [...markers]) {
			marker.style.left = (+marker.dataset.value / max * 100) + '%';
			if (+marker.dataset.value < current) {
				marker.classList.add('active');
			}
		}
	}
	
	window.addEventListener('load', initOfferBar);
	
	// следим за изменением текущей цены корзины (оффер бар) и изменяем шкалу
	new MutationObserver((mutations) => {
		for (const mutation of mutations) {
			const max = +mutation.target.dataset.max;
			const current = (+mutation.target.dataset.current > max) ? max : +mutation.target.dataset.current;
	
			if (current <= 0) {
				setTimeout(() => {
					scale.style.opacity = 0;
				}, 320);
			} else {
				scale.style.opacity = 1;
			}
	
			scale.style.width = (current / max * 100) + '%';
	
			for (const marker of [...markers]) {
				marker.classList[+marker.dataset.value < current ? 'add' : 'remove']('active');
			}
		}
	}).observe(offerBar, {attributes: true, classList: false, subtree: false});

	window.changeOfferBar = changeOfferBar;
}
