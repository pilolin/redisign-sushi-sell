import {setProduct} from './api/product';

// кнопки +/- у товара в корзине в шапке
document.addEventListener('click', (e) => {
	if( !e.target.closest('[data-header-cart-product-amount] button') ) return;

	const product = e.target.closest('[data-product-id]');

	if(!product) return;

	const productId = product.dataset.productId;
	const composite = product.dataset.compositeId ? { 
		foods_id_str: product.dataset.compositeFoods, 
		composite_id: product.dataset.compositeId 
	} : {};
	const targetButton = e.target.closest('.form-item__number button');
	const field = e.target.closest('.form-item__number');
	const input = field.querySelector('input');
	const productInCatalog = document.querySelector(`.products .product-item[data-product-id="${productId}"]`);
	let newAmount = +input.value;
	
	if (targetButton.dataset.type === 'add') {
		newAmount = +input.value + 1;
	} else {
		newAmount = (+input.value > 1) ? +input.value - 1 : 0;
	}

	setProduct(productId, newAmount, composite).then((r) => {

		if( !product.dataset.compositeId ) {
			if(newAmount) {
				input.value = newAmount;
			} else {
				productInCatalog.classList.remove('product-item_in-cart');
			}

			if(productInCatalog) {
				productInCatalog.querySelector(`[data-product-field-amount] input`).value = newAmount;
			}
		}
	});
});