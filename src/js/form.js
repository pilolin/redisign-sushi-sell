// focus text field
document.addEventListener('focusin', (e) => {
	if ( !e.target.closest('.form-item__text-field input') ) return;

	e.target.closest('.form-item__text-field').classList.add('edit');
	e.target.closest('.form-item__text-field').classList.add('focus');
});

// change autofill text field
document.addEventListener('change', (e) => {
	if ( !e.target.closest('.form-item__text-field input') ) return;

	if ( !e.target.value.length ) return;

	e.target.closest('.form-item__text-field').classList.add('edit');
	e.target.closest('.form-item__text-field').classList.add('focus');
});

// blur text field
document.addEventListener('focusout', (e) => {
	if ( !e.target.closest('.form-item__text-field input') ) return;

	const inputElement = e.target;

	if ( !inputElement.value.length ) {
		inputElement.parentElement.classList.remove('edit');
	}

	inputElement.parentElement.classList.remove('focus');
});

// focus text field
document.addEventListener('focusin', (e) => {
	if ( !e.target.closest('.form-item__select select') ) return;

	e.target.closest('.form-item__select').classList.add('edit');
	e.target.closest('.form-item__select').classList.add('focus');
});

// blur text field
document.addEventListener('focusout', (e) => {
	if ( !e.target.closest('.form-item__select select') ) return;

	const selectElement = e.target;

	if ( !selectElement.value.length ) {
		selectElement.parentElement.classList.remove('edit');
	}

	selectElement.parentElement.classList.remove('focus');
});

// focus textarea
document.addEventListener('focusin', (e) => {
	if ( !e.target.closest('.form-item__textarea textarea') ) return;

	e.target.closest('.form-item__textarea').classList.add('edit');
	e.target.closest('.form-item__textarea').classList.add('focus');
});

// change autofill textarea
document.addEventListener('change', (e) => {
	if ( !e.target.closest('.form-item__textarea input') ) return;

	if ( !e.target.value.length ) return;

	e.target.closest('.form-item__textarea').classList.add('edit');
	e.target.closest('.form-item__textarea').classList.add('focus');
});

// blur textarea
document.addEventListener('focusout', (e) => {
	if ( !e.target.closest('.form-item__textarea textarea') ) return;

	const textareaElement = e.target.closest('.form-item__textarea textarea');

	if ( !textareaElement.value.length ) {
		textareaElement.parentElement.classList.remove('edit');
	}

	textareaElement.parentElement.classList.remove('focus');
});