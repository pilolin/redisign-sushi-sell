import {getCode, confirmCode} from './api/order';
let timer;
let timerValue = 60;
let isCodeConfirmed = false;

// проверка заполнения номера для получения смс
document.addEventListener('input', (e) => {
	if(!e.target.closest('.form-item__phone-for-code input')) return;

	const isPhoneComplite = (e.target.value.replace(/\D/g, '').length === 11);

	document.querySelector('.button-get-code')[isPhoneComplite ? 'removeAttribute' : 'setAttribute']('disabled', 'disabled');

	document.querySelector('.form-item__phone').classList[isPhoneComplite ? 'add' : 'remove']('edit');
	document.querySelector('.form-item__phone input').value = isPhoneComplite ? e.target.value : '';
});

// отправка запроса на получение кода
document.addEventListener('click', (e) => {
	if(!(e.target.closest('.button-get-code') || e.target.closest('.resend-code'))) return;

	getCode(document.querySelector('.form-item__phone-for-code input').value.replace(/\D/g, '')).then((r) => {
		if(r.result) {
			e.target.setAttribute('disabled', 'disabled');

			const getCodeEl = document.querySelector('.get-code');
			const confirmCodeEl = document.querySelector('.confirm-code');

			getCodeEl.style.display = 'none';
			confirmCodeEl.style.display = 'grid';

			timer = setInterval(() => {
				if(--timerValue > 0) {
					confirmCodeEl.querySelector('strong').innerHTML = `Отправить код ещё раз через ${timerValue} сек.`;
				} else {
					timer = clearInterval(timer);
					timerValue = 60;
					confirmCodeEl.querySelector('strong').innerHTML = '<a href="javascript:void(0);" class="resend-code">Отправить код повторно</a>';
				}
			}, 1000)
		}
	});
});

// отправка запроса на подтверждение кода
document.addEventListener('click', (e) => {
	if(!e.target.closest('.button-confirm-code')) return;

	const confirmCodeEl = document.querySelector('.confirm-code');

	timer = clearInterval(timer);
	timerValue = 60;
	confirmCodeEl.querySelector('strong').innerHTML = '';

	confirmCode(document.querySelector('.form-item__phone-for-code input').value.replace(/\D/g, ''), document.querySelector('.form-item__code input').value).then((r) => {
		if(r.result) {
			document.querySelector('.checkout-total__submit').removeAttribute('disabled');
			isCodeConfirmed = true;
			confirmCodeEl.querySelector('.form-item__code .form-group').classList.remove('has-error');
			if (confirmCodeEl.querySelector('.form-item__code .form-item__message')) {
				confirmCodeEl.querySelector('.form-item__code .form-item__message').remove();
			}
		} else {
			confirmCodeEl.querySelector('.form-item__code .form-group').classList.add('has-error');
			if (!confirmCodeEl.querySelector('.form-item__code .form-item__message')) {
				confirmCodeEl.querySelector('.form-item__code').insertAdjacentHTML('beforeend', '<div class="form-item__message">Неверный код</div>');
			}
		}
	});
});

if(document.querySelector('[data-tab-type="call"]')) {
	new MutationObserver((mutations) => {
		for (const mutation of mutations) {
			const submitbtnOrder = document.querySelector('.checkout-total__submit');
	
			(mutation.target.getAttribute('aria-hidden') === 'true' && !isCodeConfirmed ) ? 
			submitbtnOrder.setAttribute('disabled', 'disabled') :
			submitbtnOrder.removeAttribute('disabled');
		}
	}).observe(document.querySelector('[data-tab-type="call"]'), {attributes: true, classList: false, subtree: false});
}

const checkoutDeliveryObserver = new MutationObserver(function(mutationsList, observer) {
	for (let mutation of mutationsList) {
			var totalSumElem = document.querySelector('.checkout-total__sum');
			if (mutation.target.href.includes('#pickup')) {
				const price = (mutation.target.getAttribute('aria-selected') === 'true') ? totalSumElem.dataset.withDiscount : totalSumElem.dataset.total;

				document.querySelector('.order-discount-pickup').style.display = (mutation.target.getAttribute('aria-selected') === 'true') ? 'flex' : 'none';
				totalSumElem.querySelector('span').innerHTML = price;
				changeOfferBar(price);
				document.querySelector('.header__btn-basket span').innerHTML = price;
				document.querySelector('.header-basket-products__total-sum span').innerHTML = price;
			}
		}
});

for (const tab of document.querySelectorAll('.checkout-delivery__type-delivery [role="tab"]')) {
	checkoutDeliveryObserver.observe(tab, { attributes: true, childList: false, characterData: false });
}
