window.setHeaderBasket = setHeaderBasket;

function getHtmlHeaderProduct(prod, composite = false) {
	return `
		<div class="header-basket-product" data-product-id="${prod.id}" ${composite ? 'data-composite-id="'+composite.id+'"' : ''} ${composite ? 'data-composite-foods="'+composite.foodsIds+'"' : ''}>
			<div class="header-basket-product__img"><img src="/${prod.img}" alt="${prod.name}"></div>
			<div class="header-basket-product__body">
					<div class="header-basket-product__header">
							<h3>${prod.name}</h3>
							${prod.mod ? '<span>'+prod.mod+'</span>' : ''}
							${composite ? '<div class="header-basket-product__composition">'+composite.info+'</div>' : ''}
					</div>
			</div>
			<div class="form-item__number header-basket-product__amount" data-header-cart-product-amount>
					<button data-type="remove"><i class="icon-minus"></i></button>
					<input type="text" name="amount" value="${+prod.qty}" readonly="">
					<button data-type="add"><i class="icon-plus"></i></button>
			</div>
			<div class="header-basket-product__sum"><span>${prod.price * prod.qty}</span><i class="icon-rub"></i></div>
			<button class="header-basket-product__delete icon-close" data-delete-product></button>
		</div>`;
}

// изменение корзины в хедере
function setHeaderBasket(basket) {
	if(!basket) return;

	const headerBasketEl = document.querySelector('.header__basket');

	const basketTotalEl = headerBasketEl.querySelector('.header__btn-basket > span');

	const basketAmountEl = headerBasketEl.querySelector('.header__btn-basket i span');
	const basketAmountMobEl = document.querySelector('.header__basket-mob span');

	const basketPopupEl = headerBasketEl.querySelector('.header-basket');
	const basketPopupGiftEl = basketPopupEl.querySelector('.header-basket-notify__text');
	const basketPopupListEl = basketPopupEl.querySelector('.header-basket-products__list');
	const basketPopupTotalEl = basketPopupEl.querySelector('.header-basket-products__total-sum span');

	basketTotalEl.innerHTML = basket.total;

	basketAmountEl.innerHTML = basket.amount;
	basketAmountMobEl.innerHTML = basket.amount;

	if (JSON.parse(basket.context_rule).length) {
		basketPopupEl.classList.remove('short');
		basketPopupGiftEl.innerHTML = JSON.parse(basket.context_rule);
	} else {
		basketPopupEl.classList.add('short');
	}
	basketPopupTotalEl.innerHTML = basket.total;

	let basketListHtml = ''
	for (const item of Object.values(basket.items)) {
		if(item.IsComposite) {
			for (const compositeId in item.CompositeInformation) {
				const composite = item.CompositeInformation[compositeId];

				basketListHtml += getHtmlHeaderProduct({
					id: item.id,
					img: item.MiniImageUrl,
					name: item.Name,
					qty: composite.qty,
					price: item.Price
				}, {
						id: compositeId,
						foodsIds: composite.items.map(prod => prod.id).join(','),
						info: composite.items.map(prod => prod.Name).join(', '),
				});
			}
		} else {
			basketListHtml += getHtmlHeaderProduct({
				id: item.id,
				img: item.MiniImageUrl,
				name: item.MainFood ? item.MainFood.Name : item.Name,
				mod: item.Modistring,
				qty: item.qty,
				price: item.Price
			});
		}
	}

	basketPopupListEl.innerHTML = '';
	basketPopupListEl.insertAdjacentHTML('afterbegin', basketListHtml);
}