import {debounce, convertStyleObjToStr} from './helper';

function setPositionBgActiveTab(tabs) {
	const tabsList = tabs.querySelector('.tabs-list');
	const tabsListPos = tabsList.getBoundingClientRect();
	const activeTab = tabs.querySelector('[role="tab"][aria-selected="true"]');
	const activeTabPos = activeTab.getBoundingClientRect();
	const bg = tabs.querySelector('.tabs-list__background');

	const styles = {
		top: (activeTabPos.top - tabsListPos.top) - 2 + 'px',
		left: (activeTabPos.left - tabsListPos.left) - 2 + 'px',
		width: activeTab.clientWidth + 'px',
		height: activeTab.clientHeight + 'px',
	}

	bg.setAttribute('style', convertStyleObjToStr(styles));
}

function allSetPositionBgActiveTab() {
	const tabsBlocks = document.querySelectorAll('.tabs');

	for (const tabs of tabsBlocks) {
		setPositionBgActiveTab(tabs);
	}
}

const debAllSetPositionBgActiveTab = debounce(allSetPositionBgActiveTab, 150);

window.addEventListener('load', allSetPositionBgActiveTab);
window.addEventListener('resize', debAllSetPositionBgActiveTab);

document.addEventListener('click', (e) => {
	if(!e.target.closest('[role="tab"][aria-selected="false"]')) return;

	const tabs = e.target.closest('.tabs');

	const targetTab = e.target.closest('[role="tab"][aria-selected="false"]');
	const targetContent = tabs.querySelector(`[data-tab-type="${targetTab.hash.substr(1)}"]`);

	const activeTab = tabs.querySelector(`[role="tab"][aria-selected="true"]`);
	const activeContent = tabs.querySelector(`[data-tab-type="${activeTab.hash.substr(1)}"]`);

	targetTab.setAttribute('aria-selected', 'true');
	targetContent.setAttribute('aria-hidden', 'false');

	activeTab.setAttribute('aria-selected', 'false');
	activeContent.setAttribute('aria-hidden', 'true');

	setPositionBgActiveTab(e.target.closest('.tabs'));

	e.preventDefault();
});

// табы модификаторов в товаре
function setPositionBgModificator($modInput) {
	var $modificator = $modInput.parent();
	var $modificatorBackground = $modificator.siblings('.product-item__modificator-background')
	var $positionModificator = $modificator.position();
	var $sizeModificator = {
		width: $modificator.width(),
		height: $modificator.height()
    };

	$modificatorBackground.css({
		top: $positionModificator.top,
		left: $positionModificator.left,
		width: $sizeModificator.width,
		height: $sizeModificator.height
	});
}

function allSetPositionBgModificator() {
	var modificators = $('.product-item__modificator input:checked');

	modificators.each(function(i, mod) {
        setPositionBgModificator($(mod));
        mod.click();
	});
}

var debAllSetPositionBgModificator = debounce(allSetPositionBgModificator, 150);

$(window).on('load', allSetPositionBgModificator);
$(window).on('resize', debAllSetPositionBgModificator);

$(document).on('change', '.product-item__modificator input', function(e) {
	e.preventDefault();

	setPositionBgModificator($(this));
});