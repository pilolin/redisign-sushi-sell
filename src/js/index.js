if(process.env.NODE_ENV === 'development') {
	const $ = require('jquery');
	window.$ = window.jQuery = $;
}

import {hideScroll, showScroll} from './helper';
import './tabs';
import './materialize/global';
import './materialize/anime.min';
import './materialize/cash';
import './materialize/modal';
import './materialize/timepicker';
import './materialize/datepicker';
import Inputmask from 'inputmask';
import './form';
import lozad from 'lozad'
import 'magnific-popup/dist/jquery.magnific-popup';
import './header-cart';
import './header-cart-actions';
import './offer-bar';
import './main';
import './basket';
import './product';
import './composite-products';
import './product-actions';
import './order';
import { setCity } from './api/general';
import { getCart } from './api/general';
import './../scss/styles.scss';

new Inputmask('+7(999) 999-99-99', {showMaskOnHover: false}).mask('[type="tel"], mask-tel');

// lazyload
lozad('.lazy', {
	loaded: function(el) {
		el.classList.add('loaded');
	}
}).observe();

document.addEventListener('DOMContentLoaded', function() {
	const elemsDatepicker = document.querySelectorAll('.materialize-datepicker input');
	M.Datepicker.init(elemsDatepicker);

	const elemsTimepicker = document.querySelectorAll('.materialize-timepicker input');
	M.Timepicker.init(elemsTimepicker);
});

document.getElementById('header-burger').addEventListener('click', (e) => {
	const header = document.querySelector('.header');
	const mobileMenu = document.getElementById('mobile-menu');
	const opened = (mobileMenu.getAttribute('aria-expanded') === 'true');

	mobileMenu.setAttribute('aria-expanded', !opened);

	if(opened) {
		header.classList.remove('opened-mobile-menu');
		showScroll();
	} else {
		header.classList.add('opened-mobile-menu');
		hideScroll();
	}
}, {passive: true});

// разворот текстовой области с комментарием по адресу в оформлении заказа
document.addEventListener('change', (e) => {
	if(e.target.id !== "is-comment-address") return;

	const commentAddress = document.querySelector('.form-item__textarea-address-comment');

	commentAddress.setAttribute('aria-hidden', !e.target.checked);
	commentAddress.style.display = e.target.checked ? 'block' : 'none';
});

// разворот текстовой области с комментарием по сдаче в оформлении заказа
document.addEventListener('change', (e) => {

	if(!e.target.closest('.checkout-payment [type="radio"]')) return;

	const cashRadio = document.querySelector('#payment-1');
	const cashField = document.querySelector('.form-item__text-field-cash');

	cashField.setAttribute('aria-hidden', !cashRadio.checked);
	cashField.style.display = cashRadio.checked ? 'block' : 'none';
});

document.addEventListener('click', (e) => {
	if(!e.target.closest('.mobile-app-notification__close')) return;

	e.target.closest('.mobile-app-notification').remove();
})

document.addEventListener('click', (e) => {
	if (!e.target.closest('.header-cities a')) return;

	setCity(e.target.dataset.id).then((r) => {
		if (r) {
			setCityInHeader(e.target.dataset.id);
		}
	});
});

document.addEventListener('change', (e) => {
	if (!e.target.closest('.mobile-menu__city')) return;

	setCity(targetCityId).then((r) => {
		if (r) {
			setCityInHeader(e.target.value);
		}
	});
});

function setCityInHeader(cityId) {
	// desktop
	const componentEl = document.querySelector('.header-cities');
	const currentEl = componentEl.querySelector('.header-cities__current span');
	const selectedItemEl = componentEl.querySelector('li[aria-selected="true"]');
	const newSelectedEl = componentEl.querySelector(`[data-id="${cityId}"]`).parentElement;
	const newSelectedText = componentEl.querySelector(`[data-id="${cityId}"]`).innerHTML;

	// mobile
	const selectEl = document.querySelector('.mobile-menu__city');

	currentEl.innerHTML = newSelectedText;
	selectedItemEl.setAttribute('aria-selected', 'false');
	newSelectedEl.setAttribute('aria-selected', 'true');

	selectEl.value = cityId;
}
document.addEventListener('change', (e) => {
	if (!e.target.closest('.form-item__switch-type-time')) return;

	const isFast = e.target.closest('.form-item__switch-type-time input').checked;
	const dateEl = document.querySelector('.checkout-time__date');

	dateEl.setAttribute('aria-hidden', isFast ? 'true' : 'false');
});

document.addEventListener('click', (e) => {
	if (!e.target.closest('.footer-main-content-list__title')) return;

	const container = e.target.closest('.footer-main-content-list');
	const ul = container.querySelector('ul');
	const opened = container.classList.contains('opened');

	if (opened) {
		container.classList.remove('opened');
		ul.style.height = '0px';
	} else {
		container.classList.add('opened');
		ul.style.height = ul.scrollHeight + 'px';
	}
});

$('.cart-modal_close').on('click', () => {
	$.magnificPopup.close();
});

document.addEventListener('DOMContentLoaded', () => {
	getCart();
})