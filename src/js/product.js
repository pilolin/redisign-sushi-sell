function getHtmlModal(product) {
	let modsElems = [];
	if (product.mods.length) {
		for (const mod of product.mods) {
			modsElems.push(`
				<label class="product-item__modificator">
					<input type="radio" ${product.selectedMod === mod.id ? 'checked' : ''} name="mod" value="${mod.id}">
					<span>${mod.name}</span>
				</label>`);
		}
	}

	return `
		<div class="cb-modal modal-product" data-product-id="${product.id}">
			<div class="cb-modal-wrap">
				<div class="modal-product__img product-item__img">
					<img src="${product.imgSrc}">
					<div class="product-item__tags">${product.$tagsEl.length ? product.$tagsEl.html() : ''}</div>
				</div>
				<div class="modal-product__content">
					<div class="product-item__content">
						<div class="product-item__name">${product.name}</div>
						${product.$componentsEl.length ? '<div class="product-item__components">' + product.$componentsEl.html() + '</div>' : ''}
						${product.mods.length ? '<div class="product-item__modificators">' + modsElems.join('') + '<div class="product-item__modificator-background"></div></div>' : ''}
						${product.$propsEl.length ? '<div class="product-item__props">' + product.$propsEl.html() + '</div>': ''}
					</div>
					<div class="product-item__footer">
						<div class="product-item__price"><span>${product.price}</span><i class="icon-rub"></i></div>
						<div class="product-item__actions">
							<button class="btn btn_type_gradient-red btn_size_sm product-item__btn" data-btn-add-to-cart ${product.inCart ? 'style="display:none;"' : ''}><span>Заказать</span></button>
							<div class="form-item__number product-item__amount form-item__number_size_lg" data-product-field-amount ${product.inCart ? 'style="display:flex;"' : ''}>
								<button data-type="remove"><i class="icon-minus"></i></button>
								<input type="text" name="cat-1-606" value="${product.count}" readonly>
								<button data-type="add"><i class="icon-plus"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>`;
}

$('.product-popup').on('click', function() {
	if($(this).closest('.product-item_composite').length) return;

	const $productEl = $(this).closest('.product-item');

	if(document.documentElement.clientWidth > 577) {
		const modal = getHtmlModal({
			id:							$productEl.attr('data-product-id'),
			imgSrc:					$productEl.find('.product-item__img img').attr('src'),
			$tagsEl:				$productEl.find('.product-item__tags'),
			name:						$productEl.find('.product-item__name').html(),
			$componentsEl:	$productEl.find('.product-item__components'),
			$propsEl:				$productEl.find('.product-item__props'),
			mods:						[...$productEl.find('.product-item__modificators input')].map((e) => { return {id: e.value, name: e.nextElementSibling.innerHTML} }),
			selectedMod:		$productEl.find('.product-item__modificators input:checked').val(),
			price:					$productEl.find('.product-item__price span').html(),
			count:					$productEl.find('[data-product-field-amount] input').val(),
			inCart:					$productEl.hasClass('product-item_in-cart'),
		});

		$.magnificPopup.open({
			items: { src: modal },
			type: 'inline',
			callbacks: {
				open: () => {
					const $modal = $('.modal-product');
					const $mod = $modal.find('.product-item__modificators input:checked').parent()
					
					if ($mod.length) {
						const $modBg = $modal.find('.product-item__modificator-background');
						const positionMod = $mod.position();
						const sizeMod = {
							width: $mod.width(),
							height: $mod.height()
						};
					
						$modBg.css({
							top: positionMod.top,
							left: positionMod.left,
							width: sizeMod.width,
							height: sizeMod.height
						});
					}
				},
				beforeClose: () => {
					const $modal = $('.modal-product');
					const $product = $(`.product-item[data-product-id="${$modal.attr('data-product-id')}"]`);

					if($product.length) {
						const amount = $modal.find('.product-item__amount input').val();

						$product.find('.product-item__amount input').val(amount);

						$product[amount > 0 ? 'addClass' : 'removeClass']('product-item_in-cart');	
					}
				}
			}
		}, 0);

	} else {
		// на мобилке
		$productEl.toggleClass('product-item_opened')
							.siblings('.product-item_opened')
							.removeClass('product-item_opened');
	}
});
