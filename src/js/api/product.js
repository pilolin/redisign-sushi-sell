export function getProduct(id) {
	return new Promise((resolve, reject) => {
		$.post({
			method: 'POST',
			url: '/shop/get-product',
			data: { id },
			success: resolve,
			error: reject
		})
	})
}

export function setProduct(id, quantity, payload) {
	return new Promise((resolve, reject) => {
		$.post({
			method: 'POST',
			url: '/cart/set-product',
			data: { id, quantity, ...payload },
			success: (r) => {
				setHeaderBasket(r)
				if(changeOfferBar) {
					changeOfferBar(r.total);
				}
				// если страница корзины то обновляем корзину
				if(document.querySelector('.basket-catalog')) {
					updateBasket(r);
					updateExtraProductBasket(r.free_items);
					document.querySelector('.basket-total__sum span').innerHTML = r.total;
				}
				resolve(r);
			},
			error: reject
		})
	})
}

export function getCompositeProducts(id, composite_id, foods_str) {
	return new Promise((resolve, reject) => {
		$.post({
			method: 'POST',
			url: '/cart/rendercompositeitems',
			data: { id, composite_id, foods_str },
			success: resolve,
			error: reject
		})
	})
}

export function addWishlist(id) {
	return new Promise((resolve, reject) => {
		$.post({
			method: 'POST',
			url: '/cart/add-wishlist',
			data: { id },
			success: resolve,
			error: reject
		})
	})
}