export function setCity(cityId) {
	return new Promise((resolve, reject) => {
		$.post({
			method: 'POST',
			url: '/site/set-city',
			data: { cityId },
			success: resolve,
			error: reject
		})
	})
}

export function getCart() {
	return new Promise((resolve, reject) => {
		$.post({
			method: 'POST',
			url: '/cart/get-cart',
			data: { },
			success: (r) => {
				setHeaderBasket(r);
				if(changeOfferBar) {
					changeOfferBar(r.total);
				}
				resolve(r);
			},
			error: reject
		})
	})
}

export function setAdditionalItems(items) {
	return new Promise((resolve, reject) => {
		$.post({
			method: 'POST',
			url: '/cart/additional-items',
			data: { ...items },
			success: resolve,
			error: reject
		})
	})
}