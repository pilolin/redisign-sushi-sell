export function getCode(phone) {
	return new Promise((resolve, reject) => {
		$.post({
			method: 'POST',
			url: '/cart/get-code',
			data: { phone },
			success: resolve,
			error: reject
		})
	})
}

export function confirmCode(phone, code) {
	return new Promise((resolve, reject) => {
		$.post({
			method: 'POST',
			url: '/cart/confirm-code',
			data: { phone, code },
			success: resolve,
			error: reject
		})
	})
}