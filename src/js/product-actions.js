import { getProduct, setProduct } from './api/product';
import {setAdditionalItems} from './api/general';

// первоначальное добавление в корзину
document.addEventListener('click', (e) => {
	if( !e.target.closest('[data-btn-add-to-cart]') ) return;

	const product = e.target.closest('[data-product-id]');

	if(!product) return;

	const productId = product.dataset.productId;

	setProduct(productId, 1).then((r) => {
		product.classList.add('product-item_in-cart');
		product.querySelector('[data-product-field-amount] input').value = r.items[productId].qty;

		// если добавление из модалки товара то меняем и у товара 
		if(product.classList.contains('modal-product')) {
			const productInCatalog = document.querySelector(`.products .product-item[data-product-id="${productId}"]`);

			productInCatalog.classList.add('product-item_in-cart');
			productInCatalog.querySelector('[data-product-field-amount] input').value = r.items[productId].qty;
		}
	});
});

// кнопки +/- у товара в каталоге
document.addEventListener('click', (e) => {
	if( !e.target.closest('[data-product-field-amount] button') ) return;

	const product = e.target.closest('[data-product-id]');

	if(!product) return;

	const productId = product.dataset.productId;
	const targetButton = e.target.closest('.form-item__number button');
	const field = e.target.closest('.form-item__number');
	const input = field.querySelector('input');
	let newAmount = +input.value;
	
	if (targetButton.dataset.type === 'add') {
		newAmount = +input.value + 1;
	} else {
		newAmount = (+input.value > 1) ? +input.value - 1 : 0;
	}

	setProduct(productId, newAmount).then((r) => {
		// если количество 0
		if(!newAmount) {
			product.classList.remove('product-item_in-cart');
		}

		input.value = newAmount;

		// если добавление из модалки товара то меняем и у товара 
		if(product.classList.contains('modal-product') && +input.value) {
			if(document.querySelector(`.products .product-item[data-product-id="${productId}"] [data-product-field-amount] input`)) {
				document.querySelector(`.products .product-item[data-product-id="${productId}"] [data-product-field-amount] input`).value = input.value;
			}
		}
	});
});

// добавление в избранное продукта
document.addEventListener('click', (e) => {
	if( !e.target.closest('[data-product-like]') ) return;

	const product = e.target.closest('[data-product-id]');

	if(!product) return;

	const productId = product.dataset.productId;

	// addWishlist(productId).then((r) => {
		const likeBtn = product.querySelector('.product-item__like i');
		const isSelected = likeBtn.classList.contains('icon-heart');

		likeBtn.classList[isSelected ? 'remove' : 'add']('icon-heart');
		likeBtn.classList[isSelected ? 'add' : 'remove']('icon-heart-outline');
	// });
});

// удаление товара
document.addEventListener('click', (e) => {
	if( !e.target.closest('[data-delete-product]') ) return;

	const product = e.target.closest('[data-product-id]');

	if(!product) return;

	const productId = product.dataset.productId;
	const composite = product.dataset.compositeId ? { 
		foods_id_str: product.dataset.compositeFoods, 
		composite_id: product.dataset.compositeId 
	} : {};

	setProduct(productId, 0, composite).then((r) => {
		if( !product.dataset.compositeId ) {
			const productInCatalog = document.querySelector(`.products .product-item[data-product-id="${productId}"]`);

			if(productInCatalog) {
				productInCatalog.classList.remove('product-item_in-cart');
				productInCatalog.querySelector('[data-product-field-amount] input').value = 0;
			}
		}

		// если страница корзины то обновляем корзину
		if(document.querySelector('.basket-catalog')) {
			document.querySelector(`.basket-catalog [data-product-id="${productId}"]`).remove();
			updateExtraProductBasket(r.free_items);
		}
	});
});


// кнопки +/- у суб товаров в корзине
document.addEventListener('click', (e) => {
	if( !e.target.closest('[data-extra-product-amount] button') ) return;

	const sticks = document.querySelector('.tools-list_img--sticks input');
	const sous = document.querySelector('.tools-list_img--sous input');
	const wasabi = document.querySelector('.tools-list_img--vasabi input');
	const ginger = document.querySelector('.tools-list_img--ginger input');

	const targetButton = e.target.closest('.form-item__number button');
	const field = e.target.closest('.form-item__number');
	const input = field.querySelector('input');
	
	if (targetButton.dataset.type === 'add') {
		input.value = +input.value + 1;
	} else {
		input.value = (+input.value > +input.dataset.min) ? +input.value - 1 : +input.dataset.min;
	}

	setAdditionalItems({
		chopsticks: sticks.value,
		sauce: sous.value,
		wasabi: wasabi.value,
		ginger: ginger.value,
	}).then((r) => {
		try {
			document.querySelector('.header__btn-basket span').innerHTML = r.total;
			document.querySelector('.header-basket-products__total-sum span').innerHTML = r.total;
			document.querySelector('.basket-total__sum span').innerHTML = r.total;
		} catch (error) {}
	});
});

// добавление с банера товара
document.addEventListener('click', (e) => {
	if (!e.target.closest('.product-card-promo[data-product-id] a')) return;

	const productId = e.target.closest('.product-card-promo[data-product-id]').dataset.productId;

	setProduct(productId, 1).then((r) => {
		const productInCatalog = document.querySelector(`.products .product-item[data-product-id="${productId}"]`);

		if(productInCatalog) {
			productInCatalog.classList.add('product-item_in-cart');
			productInCatalog.querySelector('[data-product-field-amount] input').value = 1;
		}
	});
});