window.updateBasket = updateBasket;

// изменение корзины
function updateBasket(basket) {
	if(!basket) return;

	const basketCatalog = document.querySelector('.basket-catalog');

	for (const item of Object.values(basket.items)) {
		if(item.IsComposite) {
			for (const compositeId in item.CompositeInformation) {
				const product = basketCatalog.querySelector(`[data-product-id="${item.id}"]['data-composite-id="${compositeId}"]`);

				product.querySelector('[data-product-field-amount] input').value = item.qty;
				product.querySelector('.basket-catalog-item__total span').innerHTML = item.Price * +item.qty;
			}
		} else {
			const product = basketCatalog.querySelector(`[data-product-id="${item.id}"]`);

			product.querySelector('[data-product-field-amount] input').value = item.qty;
			product.querySelector('.basket-catalog-item__total span').innerHTML = item.Price * +item.qty;
		}
	}

	deleteZeroProductInBasket();

	// заменяем подарок на тот что в ответе пришел
	if (document.querySelector('.basket-catalog-item_free')) {
		document.querySelector('.basket-catalog-item_free').remove();
	}

	if (typeof basket.gift === 'object' && Object.keys(basket.gift).length) {
		basketCatalog.insertAdjacentHTML('beforeend', htmlGiftInBasket(basket.gift));
	}
}

function deleteZeroProductInBasket() {
	setTimeout(() => {
		for (const prod of [...document.querySelectorAll('.basket-catalog .basket-catalog-item')]) {
			if (prod.querySelector('.basket-catalog-item__amount input').value == 0) {
				prod.remove();
			}
		}
	}, 0)
}

function htmlGiftInBasket(gift) {
	return `<li class="basket-catalog-item basket-catalog-item_free" data-product-id="339">
						<div class="basket-catalog-item__img">
								<a href="javascript:void(0);"><img src="${gift.img}" alt="${gift.title}"></a>
						</div>
						<div class="basket-catalog-item-content">
							<div class="basket-catalog-item-header">
								<a href="javascript:void(0);">
									<h3 class="basket-catalog-item__name">${gift.title}</h3>
								</a>
							</div>
							<div class="basket-catalog-item__components">${gift.desc}</div>
						</div>
						<div class="basket-catalog-item__price"> <span>0</span><i class="icon-rub"></i></div>
						<div class="basket-catalog-item__total">Бесплатно в подарок</div>
					</li>`
}

window.updateExtraProductBasket = updateExtraProductBasket;

function updateExtraProductBasket(freeProducts) {
	const sticks = document.querySelector('.tools-list_img--sticks');
	const sous = document.querySelector('.tools-list_img--sous');
	const wasabi = document.querySelector('.tools-list_img--vasabi');
	const ginger = document.querySelector('.tools-list_img--ginger');

	sticks.querySelector('.extra-product__free strong').innerHTML = `${freeProducts && freeProducts.chopsticks ? freeProducts.chopsticks : 0} шт.`;
	sous.querySelector('.extra-product__free strong').innerHTML = `${freeProducts && freeProducts.sauce ? freeProducts.sauce : 0} шт.`;
	wasabi.querySelector('.extra-product__free strong').innerHTML = `${freeProducts && freeProducts.wasabi ? freeProducts.wasabi : 0} шт.`;
	ginger.querySelector('.extra-product__free strong').innerHTML = `${freeProducts && freeProducts.ginger ? freeProducts.ginger : 0} шт.`;

	sticks.querySelector('input').dataset.min = freeProducts && freeProducts.chopsticks ? freeProducts.chopsticks : 0;
	sous.querySelector('input').dataset.min = freeProducts && freeProducts.sauce ? freeProducts.sauce : 0;
	wasabi.querySelector('input').dataset.min = freeProducts && freeProducts.wasabi ? freeProducts.wasabi : 0;
	ginger.querySelector('input').dataset.min = freeProducts && freeProducts.ginger ? freeProducts.ginger : 0;
}